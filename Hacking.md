# Hacking guide

## Build it

See Quick start in the Readme.

## Translations

+ The `CMakeLists.txt` adds the compiler option -DLANG_`<lang-code>` as a flag.
+ `include/thenewworld/translations.hpp` dispatches this flag
+ Duplicate one existing translation in
  `include/thenewworld/tranlations/` and add the translations.
+ Add the **ISO 639-1** language code to the dispatching header and
  change the `CMakeLists.txt`
+ Recompile

## Assets

+ The `CMakeLists.txt` defines the macro `compile_asset`. For it to
  work, xxd is required.
+ Produces a header file, that contains two variables:
  + a char array
  + a size_t length of that array
+ Include the header and use the array. It will be baked into the
  executable.
