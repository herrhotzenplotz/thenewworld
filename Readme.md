# The New World

A memory game for musical nerds.

Self-contained executable. Just copy and it works out of the box.

## Requirements

+ [SDL2](https://www.libsdl.org/download-2.0.php)
+ [SDL2_ttf](https://www.libsdl.org/projects/SDL_ttf/)
  + Depends on: [Freetype 2](https://www.freetype.org/)
+ [SDL2_image](https://www.libsdl.org/projects/SDL_image/)
  + Requirement: [libpng](http://www.libpng.org/pub/png/libpng.html)
+ [yaml-cpp](https://github.com/jbeder/yaml-cpp)
+ [Boost](https://www.boost.org/)
  + asio
  + filesystem
  + date/time
  + iostreams
+ [minimp3](https://github.com/lieff/minimp3)
  + is automatically pulled in on a recursive clone
+ [xxd](https://github.com/vim/vim/blob/master/src/xxd/xxd.c)
  + required for embedding assets
+ [ncurses](https://invisible-island.net/ncurses/announce.html)
## Quick start

### Unix-alike
```bash
$ git clone --recursive git@github.com:herrhotzenplotz/thenewworld
$ cd thenewworld/
$ mkdir build
$ cd build/
$ CXX=clang++ cmake ..
$ make
$ ./thenewworld
# or for the editor
$ ./thenewworld editor
```

**Important note**: Using clang is currently necessary because GCC 9
messes up the audio with optimizations enabled. The Windows build,
which uses GCC 5 on MXE (see below) works just fine.

### Windows
The project compiles and runs on Windows (as of the latest commit on
this Readme). I haven't tested compiling it on Windows yet, but a
cross compile using [MXE](https://mxe.cc/) definitely works and is
what I am currently doing.  Please note that I try to support Windows
but it is definitely not the goal of the project because I don't
really care about the Windows ecosystem. If something breaks, either
create an issue or fix it and create a PR. See contributions below.

```powershell
.\thenewworld.exe # for the game
.\thenewworld.exe editor # for the editor
```

## Contributions

Contributions are very much welcome. Refer to the issues tab if you
are looking for an issue to start with.
Please keep in mind:
+ PR's with line changes greater than 300 lines are rejected.
+ PR's with change requests that are not adressed within 2 weeks are
  rejected.
+ Create TODO's in the source code if necessary. The repository owner
  will convert them into issues on PR submission.
+ The repository owner may deviate from this policy in special cases.
+ PRs fixing typos are rejected. Just open an issue and after some
  time I will fix all of them in one go.

## License
The source code is published under the MIT license. Please refer to
the LICENSE file for more information.


## Why?

A friend of mine, a student of choral conducting, had a wish for his
next birthday: He wanted a memory-like game where you had to match
exerpts from music pieces. This is the result.

## Acknowledgements
+ Font used in the project: [Feedjique](https://fontlibrary.org/en/font/feedjique)
+ Thanks to my friend Rocky for creating the intro music
+ [minimp3](https://github.com/lieff/minimp3) provided a suckless and
  usable mp3 implementation. Strongly recommend it instead of the
  painful, unusable and stupid API of libavcodec/libavutil. Check it
  out and leave a star!
