## Level Format description

+ Levels are stored in YAML inside the `data/` folder
+ Format:
```yaml
- file: filename.mp3
  title: Free hacker song
  movement: Introduction
  composer: FSF
  samples:
	- start: 0:0:2.0
	  end: 0:0:10.0
	- start: 0:3:39.0
	  end: 0:3:45.0
```
+ movement node may be omitted
