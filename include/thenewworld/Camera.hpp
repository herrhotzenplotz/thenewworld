/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <map>
#include <memory>
#include <string>
#include <thenewworld/Color.hpp>
#include <thenewworld/Rect.hpp>
#include <thenewworld/SDLImage.hpp>
#include <thenewworld/Vec2.hpp>
#include <vector>

namespace thenewworld {
class Camera {
public:
    Camera(SDL_Window* window, bool force_opengl = true);
    ~Camera();

    void present() const;

    void fill_screen(const Color);
    void clear_screen();
    Rect get_viewport() const;
    int render_width() const;
    void draw_filled_rect(const Rect rect, const Color color);
    void draw_rect(const Rect rect, const Color color);
    Rect text_boundaries(const std::string&) const;
    void render_text(std::string, Color, Rect dst_rect);
    void render_image(SDLImage*, Rect);
    SDLImage* create_image(std::string path);
    SDLImage* create_image(void* buffer, size_t buffer_size);

    void set_black_white_mode(bool v) { m_black_white_mode = v; }

    vec2 translation() const;
    void push_translation(vec2);
    void pop_translation();

    const SDL_Rect to_sdl_rect(const Rect) const;
    const SDL_Color to_sdl_color(const Color sys_col) const;

private:
    void create_renderer_with_id(SDL_Window*, int);
    SDL_Renderer* m_renderer;
    std::map<int, TTF_Font*> m_fonts = { { 50, nullptr }, { 100, nullptr }, { 300, nullptr } };
    bool m_black_white_mode = false;
    std::vector<vec2> m_translation_stack = {};
};
} // thenewworld
