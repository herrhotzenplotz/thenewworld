/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <cassert>

#include <thenewworld/Camera.hpp>
#include <thenewworld/Color.hpp>
#include <thenewworld/Rect.hpp>
#include <thenewworld/Vec2.hpp>

namespace thenewworld {

template<class T>
constexpr const T& clamp(const T& v, const T& lo, const T& hi)
{
    assert(!(hi < lo));
    return (v < lo) ? lo : (hi < v) ? hi : v;
}

template<typename MenuEntryType>
struct MenuEntry {
public:
    MenuEntry() = default;
    MenuEntry(const MenuEntry&) = default;
    virtual ~MenuEntry() { }
    const std::string title;
    MenuEntryType type;

protected:
    MenuEntry(const std::string title, MenuEntryType type)
	: title(title)
	, type(type)
    {
    }
};

enum class PositionPolicy { Centered };

template<typename T>
class Menu final {
public:
    Menu() = default;

    Menu(Menu&&) = default;
    ~Menu() { }

    void event(SDL_Event& e)
    {
	if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_DOWN) {
	    m_current_selection = thenewworld::clamp(m_current_selection + 1,
		0, (int)m_entries.size() - 1);
	} else if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_UP) {
	    m_current_selection = thenewworld::clamp(m_current_selection - 1,
		0, (int)m_entries.size() - 1);
	}
    }

    void update(int)
    {
    }

    void render(Camera& cam)
    {
	auto vp = cam.get_viewport();
	for (size_t i = 0; i < m_entries.size(); ++i) {
	    auto r = cam.text_boundaries(m_entries[i]->title);
	    r.scale_by(m_entry_height / r.h);

	    r.x = m_position_policy == PositionPolicy::Centered
		? (vp.w - r.w) * 0.5
		: 0.0;

	    r.y = m_rect.y + (double)i * (m_entry_height + m_padding);
	    cam.render_text(m_entries[i]->title, m_text_color, r);
	    if (m_largest_entry_rect.w < r.w)
		m_largest_entry_rect = r;
	}

	auto selection_rect = m_largest_entry_rect;
	selection_rect.y = m_rect.y + m_current_selection * (m_padding + m_entry_height);

	selection_rect.x = m_position_policy == PositionPolicy::Centered
	    ? (vp.w - m_largest_entry_rect.w) * 0.5
	    : 0.0;

	selection_rect.scale_by(1.1);

	m_rect.x = m_largest_entry_rect.x;
	m_rect.w = m_largest_entry_rect.w;
	m_rect.h = m_entries.size() * (m_padding + m_entry_height);

	cam.draw_rect(selection_rect, m_text_color);
    }

    void set_y_position(double y)
    {
	m_rect.y = y;
    }

    void set_padding(double padding)
    {
	m_padding = padding;
    }

    void set_entry_height(double entry_height)
    {
	m_entry_height = entry_height;
    }

    void set_text_color(const Color col)
    {
	m_text_color = col;
    }

    void append_entry(std::unique_ptr<MenuEntry<T>>&& entry)
    {
	m_current_selection = 0;
	m_entries.push_back(std::move(entry));
    }

    bool empty() const
    {
	return m_entries.empty();
    }

    T current_selection_type() const
    {
	assert(m_current_selection >= 0 && (size_t)m_current_selection < m_entries.size());
	return m_entries[(size_t)m_current_selection]->type;
    }

    void on_mouse_hover_entries(const vec2& mouse_pos)
    {
	auto pos_in_rect = mouse_pos - m_rect.top();
	auto entry_id = (int)std::floor(pos_in_rect.y / (m_largest_entry_rect.h + m_padding));
	m_current_selection = entry_id;
    }

    void set_position_policy(PositionPolicy pol)
    {
	m_position_policy = pol;
    }

    Rect& rect()
    {
	return m_rect;
    }

    MenuEntry<T>* current_entry_ptr() const
    {
	return m_entries[(size_t)m_current_selection].get();
    }

private:
    std::vector<std::unique_ptr<MenuEntry<T>>> m_entries;
    int m_current_selection = -1;
    double m_padding = 10.0;
    Rect m_largest_entry_rect,
	m_rect;
    double m_entry_height;
    Color m_text_color;
    PositionPolicy m_position_policy;
};

} // thenewworld
