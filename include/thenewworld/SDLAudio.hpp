/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

// MP3 support
//#include <minimp3/minimp3.h>

#include <SDL2/SDL.h>
#include <boost/optional.hpp>
#include <cstdint>
#include <thenewworld/SongConfig.hpp>
#include <thenewworld/SoundSample.hpp>

namespace thenewworld {

class SDLAudioPlayer {
public:
    SDLAudioPlayer();
    ~SDLAudioPlayer();
    void play_sample(const SoundSample*);
    void stop_playback();
    SoundSample* load_sample_from_wav_file(const std::string& path);
    SoundSample* load_from_mp3_buffer(void* buffer, size_t buffer_size);
    /* NOTE: load_mp3_file returns NULL if unable to seek. */
    SoundSample* load_mp3_file(const std::string& path, std::array<OffsetSample, 2>&);
    SoundSample* load_wav_file(const std::string& path, std::array<OffsetSample, 2>&);
    SDL_AudioSpec* audio_spec() { return &m_audiospec; }

private:
    SDL_AudioSpec m_audiospec;
    SDL_AudioDeviceID m_dev;
    SDL_AudioCVT m_cvt;
};

} // thenewworld
