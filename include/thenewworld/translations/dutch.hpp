/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#define T_GAME_TITLE "De Nieuwe Wereld"
#define T_RANDOM_GAME "Willekeurig spel"
#define T_MULTIPLAYER_GAME "Multi player modus"
#define T_SINGLEPLAYER_GAME "Single player modus"
#define T_QUIT_GAME "Eruit!"
#define T_SELECT_LEVEL "Level kiezen"
#define T_ABORT_SELECT "Annuleren"
#define T_ABOUT_PAGE "Over dit game"
#define T_ERROR_HEADER "Godverdomme!"
#define T_ERROR_MESSAGE "Dit had nooit moeten gebeuren."
#define T_ERROR_BACK "Tik op het scherm voor het menu of toets q om af te sluiten."
#define T_ERROR_WAS "Het fout was:"
#define T_ERROR_REFER_LOG "Kijk ook eens naar het terminal!"

#define T_GO_TOOK "Dit heb jij binnen "
#define T_GO_MIN_AND " minuten en "
#define T_GO_SEC " seconden goed gemaakt."
#define T_GO_WON "Gefeliciteerd"
#define T_GO_BACK "Tik op het scherm voor het menu"

#define T_LE_NAME "Level bewerken"

#define T_GAME_THAT_WAS "Dit was "
#define T_SONG_BY " van "

#define T_ABOUT_HEADER "Over"
#define T_ABOUT_FOOTER "Tik op het scherm om terug te gaan naar het menu"
#define T_ABOUT_CONTENT                                             \
    {                                                               \
        "Een speeltje voor musicanten",                             \
            "Idee van een vriend van mij voor zijn verjaardag",     \
            "Geschreven door Nico Sonack",                          \
            "Intro-muziek geschreven door Rocky Joe Hoffmann",      \
            "Gebruikte bibliotheeken:",                             \
            "SDL2 (ttf, image), yaml-cpp, boost, minimp3, ncurses", \
            "Letter type: Feedjique (kijk naar Readme.md)"          \
    }

#define T_RND_Back "Terug gaan"
#define T_RND_BoardSize "Grootte van het bord"
#define T_RND_Digging "Verwarming van je huis aan het verbeteren..."

/* =============== LEVEL EDITOR =============== */

#define T_LE_FileBrowser "BESTANDEN ZOEKEN"
#define T_LE_FileBrowser_Footer "PIJLTOESTSEN voor navigatie. ENTER - Bestand kiezen. Q - Annuleren."

#define T_LE_Header "TheNewWorld Level Editor"
#define T_LE_Footer "'w' en 's' (of Pijltoetsen) om te navigeren. Q - Afsluiten. Enter om te kiezen."

#define T_LE_ConfOK "Is dit okay? [y|n|a] "

#define T_LE_SampleBegin "Begin sample: "
#define T_LE_SampleDuration "Lengte: "

#define T_LE_SampleEditor "Sampleeditor"
#define T_LE_SampleNo "Sample No. "
#define T_LE_SampleEditorFooter                                                   \
    "Pijltoetsen: boven/beneden om te navigeren, links/rechts om te veranderen. " \
    "Q - Terug. P - Gekozen sample afspelen."

#define T_LE_SampleNotExists \
    " ===   Bestand niet gevonden. Toests om terug te gaan.   === "
#define T_LE_CannotSeek \
    " ===  Kan niet in bestand zoeken. Toets om terug te gaan. === "
#define T_LE_EndPlayback \
    " ===   TOETS OM PLAYBACK TE STOPPEN.   === "

#define T_LE_SongProperties \
    { "Bestand: ",          \
        "Naam: ",           \
        "Deel: ",           \
        "Componist: ",      \
        "Samples: " };
#define T_LE_SongEditor "Songeditor"
#define T_LE_SongEditor_Footer "ENTER - Bewerken. Q - Terug."
#define T_LE_EnterToEdit "<Enter om te bewerken>"
#define T_LE_CorruptedFile "DIT BESTAND IS BESCHADIGT OF BEVAT EEN INCORRECTE SYNTAX. BEWERK HET JEZELF."

#define T_LE_EditLevel "LEVEL BEWERKEN"

#define T_LE_EditLevel_Footer                                                       \
    "Pijltoetsen om te navigeren. ENTER om te bewerken. N - Nieuw. K - Verwijderen" \
    ". X - Opslaan. Q - Terug."

#define T_LE_Sure "Zeker? [y|n] "

#define T_LE_NewLevel "Nieuw Level"
#define T_LE_Quit "Beeindigen"
#define T_LE_LevelName "Levelnaam: "
