/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#define T_GAME_TITLE "Die Neue Welt"
#define T_ABOUT_PAGE "Über dieses Spiel"
#define T_QUIT_GAME "Raus hier!"
#define T_SELECT_LEVEL "Level wählen"
#define T_ABORT_SELECT "Abbrechen"
#define T_RANDOM_GAME "Zufälliges Spiel"
#define T_MULTIPLAYER_GAME "Mehrspielermodus"
#define T_SINGLEPLAYER_GAME "Einzelspielermodus"
#define T_ERROR_HEADER "Verdammt!"
#define T_ERROR_MESSAGE "Das sollte eigentlich nicht passieren."
#define T_ERROR_BACK "Tippen, für das Hauptmenü oder q drücken um zu beenden."
#define T_ERROR_WAS "Der Fehler war:"
#define T_ERROR_REFER_LOG "Schau bitte auch mal in das Terminal!"

#define T_GO_TOOK "Dafür hast du "
#define T_GO_MIN_AND " Minuten und "
#define T_GO_SEC " Sekunden gebraucht."
#define T_GO_WON "Gewonnen"
#define T_GO_BACK "Tippe für das Hauptmenü"

#define T_LE_NAME "Level bearbeiten"

#define T_GAME_THAT_WAS "Das war "

#define T_SONG_BY " von "

#define T_ABOUT_HEADER "Über"
#define T_ABOUT_FOOTER "Tippe, um zurück zum Hauptmenü zu gehen"
#define T_ABOUT_CONTENT                                                     \
    {                                                                       \
        "Ein Spiel für Musiker",                                            \
            "Idee einer meiner Freunde als Geschenk für seinen Geburtstag", \
            "Programmiert von Nico Sonack",                                 \
            "Intro-Musik von Rocky Joe Hoffmann",                           \
            "Genutzte Bibliotheken:",                                       \
            "SDL2 (ttf, image), yaml-cpp, boost, minimp3, ncurses",         \
            "Schriftart: Feedjique (Siehe Readme.md)"                       \
    }

#define T_RND_Back "Zurück"
#define T_RND_BoardSize "Spielbrett wählen"
#define T_RND_Digging "Der Herr Gott würfelt jetzt..."

/* =============== LEVEL EDITOR =============== */

#define T_LE_FileBrowser "DATEI BROWSER"
#define T_LE_FileBrowser_Footer "Pfeiltasten zum Navigieren. ENTER - Datei wählen. Q - Abbrechen."

#define T_LE_Header "TheNewWorld Level Editor"
#define T_LE_Footer "Hinweis: 'w' und 's' (oder Pfeiltasten) zum Navigieren. Q - Beenden. Enter zum Bestätigen."

#define T_LE_ConfOK "Ist das okay? [y|n|a] "

#define T_LE_SampleBegin "Ausschnitssbeginn: "
#define T_LE_SampleDuration "Länge: "

#define T_LE_SampleEditor "Ausschnittseditor"
#define T_LE_SampleNo "Ausschnitt Nr. "
#define T_LE_SampleEditorFooter                                                  \
    "Pfeiltasten: oben/unten zum Navigieren, links/rechts zum Wert verändern. " \
    "Q - Zurück. P - Gewählten Ausschnitt abspielen."

#define T_LE_SampleNotExists \
    " ===   Datei existiert nicht. Taste drücken zum Fortfahren.   === "
#define T_LE_CannotSeek \
    " ===  Kann nicht durch Datei suchen. Taste drücken zum Fortfahren. === "
#define T_LE_EndPlayback \
    " ===   TASTE DRÜCKEN UM PLAYBACK ZU BEENDEN.   === "

#define T_LE_SongProperties \
    { "Datei: ",            \
        "Titel: ",          \
        "Satz: ",           \
        "Komponist: ",      \
        "Auschnitte: " };
#define T_LE_SongEditor "Songeditor"
#define T_LE_SongEditor_Footer "ENTER - Wert bearbeiten. Q - Zurück."
#define T_LE_EnterToEdit "<Enter zum Bearbeiten>"
#define T_LE_CorruptedFile "DIE DATEI IST BESCHÄDIGT ODER ENTHÄLT EINE INKORREKTE SYNTAX. MANUELL DEN FEHLER BEHEBEN."

#define T_LE_EditLevel "LEVEL BEARBEITEN"

#define T_LE_EditLevel_Footer                                                               \
    "Pfeiltasten zum Navigieren. ENTER um zu Bearbeiten. N - Neuer Eintrag. K - Gewählten" \
    " Eintrag löschen. X - Speichern. Q - Beenden."

#define T_LE_Sure "Sicher? [y|n] "

#define T_LE_NewLevel "Neues Level"
#define T_LE_Quit "Beenden"
#define T_LE_LevelName "Levelname: "
