/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#define T_GAME_TITLE "The New World"
#define T_ABOUT_PAGE "About this Game"
#define T_RANDOM_GAME "Random Game"
#define T_MULTIPLAYER_GAME "Multiplayer Mode"
#define T_SINGLEPLAYER_GAME "Singleplayer Mode"
#define T_QUIT_GAME "Quit Game"
#define T_SELECT_LEVEL "Select level"
#define T_ABORT_SELECT "Abort"
#define T_ERROR_HEADER "Oh boy..."
#define T_ERROR_MESSAGE "That should not have happened."
#define T_ERROR_BACK "Tap to go back to the main menu or press q to exit."
#define T_ERROR_WAS "Error was:"
#define T_ERROR_REFER_LOG "You may also want to look at the console logs."

#define T_GO_TOOK "That took you "
#define T_GO_MIN_AND " minutes and "
#define T_GO_SEC " seconds."
#define T_GO_WON "You won"
#define T_GO_BACK "Tap to go back to the menu"

#define T_LE_NAME "Level editor"

#define T_GAME_THAT_WAS "That was "

#define T_SONG_BY " by "

#define T_ABOUT_HEADER "About"
#define T_ABOUT_FOOTER "Tap to go back to the main menu"
#define T_ABOUT_CONTENT                                             \
    {                                                               \
        "A game for musical nerds",                                 \
            "Idea by one of my friends as a birthday gift",         \
            "Programmed by Nico Sonack",                            \
            "Intro music by Rocky Joe Hoffmann",                    \
            "Libraries used:",                                      \
            "SDL2 (ttf, image), yaml-cpp, boost, minimp3, ncurses", \
            "Font: Feedjique (see Readme.md)"                       \
    }

#define T_RND_Back "Go back"
#define T_RND_BoardSize "Select board size"
#define T_RND_Digging "Digging through the file system"

/* =============== LEVEL EDITOR =============== */

#define T_LE_FileBrowser "FILE BROWSER"
#define T_LE_FileBrowser_Footer "Arrows to navigate. ENTER - select file. Q - Abort."

#define T_LE_Header "TheNewWorld Level Editor"
#define T_LE_Footer "Hint: Use 'w' and 's' (or the arrow keys) to navigate. Q - Quit. Press ENTER for confirmation."

#define T_LE_ConfOK "Is this okay? [y|n|a] "

#define T_LE_SampleBegin "Sample begin: "
#define T_LE_SampleDuration "Duration: "

#define T_LE_SampleEditor "Sample editor"
#define T_LE_SampleNo "Sample no. "
#define T_LE_SampleEditorFooter                                  \
    "WASD to navigate. I - increase value. K - decrease value. " \
    "Q - go back. P - Play selected sample."

#define T_LE_SampleNotExists \
    " ===   File doesn't exists. Press any key.   === "
#define T_LE_CannotSeek \
    " ===  Unable to seek to position in audio. Press any key. === "
#define T_LE_EndPlayback \
    " ===   PRESS ANY KEY TO STOP PLAYBACK.   === "

#define T_LE_SongProperties \
    { "File: ",             \
        "Title: ",          \
        "Movement: ",       \
        "Composer: ",       \
        "Samples: " };
#define T_LE_SongEditor "Song editor"
#define T_LE_SongEditor_Footer "ENTER - edit value. Q - go back."
#define T_LE_EnterToEdit "<enter to edit>"
#define T_LE_CorruptedFile "THE FILE IS CORRUPTED OR HAS INCORRECT SYNTAX. FIX MANUALLY."

#define T_LE_EditLevel "EDITING LEVEL"

#define T_LE_EditLevel_Footer                                              \
    "WASD to navigate. ENTER to change a value. N - new entry. K - Delete" \
    " selected entry. X - Save. Q - Exit."

#define T_LE_Sure "Are you sure? [y|n] "

#define T_LE_NewLevel "New Level"
#define T_LE_Quit "Quit"
#define T_LE_LevelName "Level name: "
