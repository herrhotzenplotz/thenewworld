/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <SDL2/SDL.h>
#include <memory>
#include <string>
#include <thenewworld/Camera.hpp>

namespace thenewworld {
class SDLWindow {
public:
    SDLWindow(std::string title, bool force_opengl = true);
    SDLWindow(const SDLWindow&) = delete;
    SDLWindow(SDLWindow&&) = default;

    ~SDLWindow();

    Camera& camera() { return *m_camera; }
    void present();
    double display_scale();

    SDL_Event preprocess_sdl_event(SDL_Event e);

private:
    double calculate_display_scale();
    SDL_Window* m_window;
    std::unique_ptr<Camera> m_camera;
    double m_display_scale = 1.0;
};
} // thenewworld
