/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <boost/date_time/posix_time/posix_time.hpp>
#include <boost/optional.hpp>
#include <random>
#include <thenewworld/Rect.hpp>
#include <thenewworld/SDLAudio.hpp>
#include <thenewworld/SDLImage.hpp>
#include <thenewworld/Slide.hpp>
#include <thenewworld/SlideMatchedInfoRenderer.hpp>
#include <thenewworld/SongConfig.hpp>
#include <thenewworld/UpdateTimer.hpp>
#include <thenewworld/Vec2.hpp>
#include <vector>

using namespace boost::posix_time;

namespace thenewworld {
class Slideboard {
public:
    Slideboard(GameContext&, size_t cols, size_t rows, SongConfig*);
    Slideboard(Slideboard&&) = default;
    ~Slideboard()
    {
        for (auto* img : m_slide_images)
            delete img;
    }

    void recalculate_slides(GameContext&);
    void load_images(GameContext&);
    void event(GameContext&, SDL_Event&);
    void update(GameContext&, int dt);
    void render(Camera&);
    bool check_for_win();
    time_duration play_time();
    bool was_incorrect_match() const;
    Rect rect() const { return m_rect; }
    void set_hide_on_wrong_match(bool value) { m_hide_on_wrong_match = value; }

private:
    Slide& slide_at(size_t, size_t);
    void set_slide_at(size_t, size_t, Slide&&);
    void click_on_slides(SDLAudioPlayer&, vec2&);
    void clear_selection();
    void match_correct();
    void match_failed();
    bool check_if_matched();

    ptime m_start_time;

    Rect m_rect,
        m_slide_dims;

    double m_padding = 50.0,
           m_gap_size = 5.0;

    size_t m_cols = 20,
           m_rows = 20;

    bool m_can_click = true;
    boost::optional<Slide*> m_selected_slide_1 = boost::none,
                            m_selected_slide_2 = boost::none;

    std::vector<Slide> m_slides;
    SlideMatchedInfoRenderer m_info_renderer;
    std::vector<SDLImage*> m_slide_images;

    bool m_was_incorrect_match = false,
         m_hide_on_wrong_match = false;
};
} // thenewworld
