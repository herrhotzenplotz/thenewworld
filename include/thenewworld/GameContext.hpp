/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <boost/asio.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <memory>
#include <random>
#include <thenewworld/GameState.hpp>
#include <thenewworld/SDLAudio.hpp>
#include <thenewworld/SDLWindow.hpp>

class SDLAudioPlayer;

namespace thenewworld {
class GameContext {
public:
    GameContext(bool force_opengl = true);
    ~GameContext();
    int run();
    void set_state(std::unique_ptr<GameState>&& newstate)
    {
        m_current_state = std::move(newstate);
        m_current_state->init(*this);
        m_switching_state = true;
    }
    GameState& state() { return *m_current_state; }
    Camera& camera() { return m_window->camera(); }
    int window_width();
    int window_height();
    SDLAudioPlayer& audio_player() { return *m_audio_player; }
    void quit();
    void start_background_music();
    void stop_background_music();
    std::mt19937& prng()
    {
        return m_prng;
    }

private:
    std::unique_ptr<SDLWindow> m_window;
    std::unique_ptr<SDLAudioPlayer> m_audio_player;
    std::unique_ptr<GameState> m_current_state;
    bool m_should_quit = false,
         m_switching_state = false,
         m_is_music_running = false;
    int fps = 60;
    std::random_device m_rd;
    std::mt19937 m_prng;

    /* Stuff for the background music */
    SoundSample* m_sound_sample;
    boost::asio::io_service m_io_service;
    boost::posix_time::milliseconds* m_interval;
    boost::asio::deadline_timer* m_timer;
    std::thread m_timer_thread;
    std::function<void(const boost::system::error_code&)> m_tick;
};
} // thenewworld
