/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <cstdint>

namespace thenewworld {
struct Color {
    Color() { }
    Color(double r, double g, double b, double a);
    Color desaturate() const;

    Color interpolate(const Color& other, const int t, const int tend) const;

    static Color from_hex(uint32_t color_code)
    {
        return { ((double)(color_code >> 16) / 255.0),
            ((double)((color_code >> 8) & 0xFF) / 255.0),
            ((double)(color_code & 0xFF) / 255.0),
            1.0 };
    }

    static const Color black;
    static const Color white;
    static const Color beige;
    static const Color olive;
    static const Color maroon;
    static const Color wine;
    static const Color pastel_red;
    static const Color orange;
    static const Color gray;
    static const Color green;

    double r, g, b, a;
};

} // thenewworld
