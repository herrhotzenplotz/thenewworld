/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <SDL2/SDL.h>
#include <SDL_image.h>
#include <thenewworld/Logging.hpp>

namespace thenewworld {
class SDLInitializer {
public:
    SDLInitializer()
    {
        log_info("Initializing SDL");
        enforce(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) == 0,
            "SDL_Init failed");

        log_info("Initializing SDL_Image");
        int img_flags = IMG_INIT_PNG;

        enforce_typed(IMG_GetError,
            IMG_Init(img_flags) == img_flags,
            "IMG_Init failed");
        log_info("Initializing SDL_ttf");

        enforce_typed(TTF_GetError,
            TTF_Init() == 0,
            "TTF_Init failed");

        int num_touch = SDL_GetNumTouchDevices();
        log_info("Found " + std::to_string(num_touch) + " touch devices");
    }

    ~SDLInitializer()
    {
        log_info("Quitting SDL, TTF and IMG");
        TTF_Quit();
        IMG_Quit();
        SDL_Quit();
    }
};
} // thenewworld
