/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

namespace thenewworld {
struct vec2 {
    double x, y;
    void operator+=(const vec2& other)
    {
        x += other.x;
        y += other.y;
    }
};

constexpr vec2 operator+(const vec2& lhs, const vec2& rhs)
{
    return { lhs.x + rhs.x, lhs.y + rhs.y };
}

constexpr vec2 operator-(const vec2& lhs, const vec2& rhs)
{
    return { lhs.x - rhs.x, lhs.y - rhs.y };
}

constexpr vec2 operator*(const vec2& v, const double s)
{
    return { v.x * s, v.y * s };
}

constexpr vec2 operator/(const vec2& v, const double s)
{
    return { v.x / s, v.y / s };
}

constexpr vec2 operator*(const double s, const vec2& v)
{
    return v * s;
}
} // thenewworld
