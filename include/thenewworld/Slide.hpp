/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <memory>
#include <thenewworld/Rect.hpp>
#include <thenewworld/SDLAudio.hpp>
#include <thenewworld/SDLImage.hpp>
#include <thenewworld/SoundSample.hpp>

namespace thenewworld {
class GameContext;
class Camera;

enum class SlideAnimationState { Idle,
    Failing,
    Opening,
    Finishing };

class Slide {
public:
    Slide(int id, const SoundSample*);
    void set_dimensions(Rect&);
    void set_is_open(bool);
    bool is_open() { return m_is_open; }
    bool toggle_is_open();
    int id() { return m_id; }
    void done();
    bool is_done() { return m_is_matched; }
    void fail();
    void set_image(SDLImage*);
    void play_sample(SDLAudioPlayer&) const;
    void update(GameContext&, int dt);
    void render(Camera&);

private:
    bool m_is_open = false,
         m_is_matched = false,
         m_failed = false;
    int m_id,
        m_animation_progress = 0,
        m_animation_duration = 500;
    double m_width = 20.0,
           m_height = 20.0;

    std::string m_title;
    SlideAnimationState m_animation_state = SlideAnimationState::Idle;
    SDLImage* m_image;
    const SoundSample* m_sample;
};
} // thenewworld
