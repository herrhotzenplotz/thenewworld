/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <SDL2/SDL.h>
#include <chrono>
#include <cstdlib>
#include <functional>
#include <iomanip>
#include <iostream>
#include <sstream>
#include <string>

namespace thenewworld {

static inline std::string timestamp()
{
    auto t_c = std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    std::ostringstream str;
    str << "[" << std::put_time(localtime(&t_c), "%F %T") << "] ";
    return str.str();
}

static inline void log_info(std::string message)
{
    std::cout << timestamp() << "INFO : " << message << std::endl;
}

static inline void log_error(std::string message)
{
    std::cout << timestamp() << "ERR  : " << message << std::endl;
}

static inline void log_warn(std::string message)
{
    std::cout << timestamp() << "WARN : " << message << std::endl;
}

template<std::string (*additional_info)(void)>
static inline void enforce_impl(bool cond, std::string message, std::string file, int line)
{
    if (!cond) {
        std::cerr << file << ":" << line << ": " << timestamp() << "FAIL : " << message
                  << ": " << additional_info();
        abort();
    }
}

template<typename F = std::function<std::string(void)>>
static inline void enforce_impl(F additional_info, bool cond, std::string message, std::string file, int line)
{
    if (!cond) {
        std::cerr << file << ":" << line << ": " << timestamp() << "FAIL : " << message
                  << ": " << additional_info();
        abort();
    }
}

template<const char* (*additional_info)(void) = SDL_GetError>
static inline void enforce_impl(bool cond, std::string message, std::string file, int line)
{
    if (!cond) {
        std::cerr << file << ":" << line << ": " << timestamp() << "FAIL : " << message
                  << ": " << additional_info();
        abort();
    }
}

#define enforce(cond, msg) enforce_impl((cond), msg, __FILE__, __LINE__)
#define enforce_typed(type, cond, msg) enforce_impl<type>((cond), msg, __FILE__, __LINE__)
#define enforce_lambda(lambda, cond, msg) enforce_impl(lambda, (cond), msg, __FILE__, __LINE__)
#define enforce_msg(cond, msg) enforce_impl([]() { return ""; }, (cond), msg, __FILE__, __LINE__)

}
