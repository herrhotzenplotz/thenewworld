/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <string>
#include <thenewworld/SoundSample.hpp>
#include <vector>
#include <yaml-cpp/yaml.h>

namespace thenewworld {

class SDLAudioPlayer;
class GameContext;

struct OffsetSample {
    int id;
    double start;
    double len;
};

struct Song {
    std::string filepath;
    std::string title;
    std::string movement;
    std::string composer;
    int id;
    std::array<OffsetSample, 2> offset_samples;
    std::array<SoundSample, 2> samples;
    SoundSample* raw_samples = nullptr;
    std::string description() const;
    void load_samples(SDLAudioPlayer&);
    void free_buffers();
    friend std::string to_string(Song&);
    friend std::ostream& operator<<(Song&, std::ostream&);
};

struct SongConfig {
    SongConfig(SongConfig&& other) = default;
    std::vector<Song> songs;
    std::string name;
    void load_songs(SDLAudioPlayer&);
    size_t sample_count() const;
    void free_samples();

    static SongConfig* parse_from_file(GameContext&, std::string);
    static SongConfig* parse_unsafe(std::string);
    static SongConfig* empty();

protected:
    SongConfig() = default;
};

} // thenewworld
