/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <SDL2/SDL.h>
#include <cmath>
#include <iostream>
#include <string>
#include <thenewworld/Vec2.hpp>

namespace thenewworld {

struct Rect {
    Rect() = default;
    Rect(double w, double h, double x, double y)
        : w(w)
        , h(h)
        , x(x)
        , y(y)
    {
    }

    Rect(SDL_Rect rect)
        : w((double)rect.w)
        , h((double)rect.h)
        , x((double)rect.x)
        , y((double)rect.y)
    {
    }

    vec2 top() const
    {
        return { x, y };
    }

    Rect scaled_by(double factor) const
    {
        auto cX = x + w * 0.5;
        auto cY = y + h * 0.5;
        auto nW = w * factor;
        auto nH = h * factor;

        return { nW, nH,
            cX - nW * 0.5,
            cY - nH * 0.5 };
    }

    void scale_by(double factor)
    {
        auto new_rect = scaled_by(factor);

        w = new_rect.w;
        h = new_rect.h;
        x = new_rect.x;
        y = new_rect.y;
    }

    Rect animate_scale(int t, int tend) const
    {
        double pt = pow((double)t / (double)tend, 0.25);
        return scaled_by(1.0 - pt);
    }

    bool contains(vec2 v)
    {
        return x <= v.x && v.x <= x + w
            && y <= v.y && v.y <= y + h;
    }

    friend std::ostream& operator<<(std::ostream&, Rect&);
    friend const Rect operator+(const Rect&, const vec2&);

    double w, h, x, y;
};

} // thenewworld
