/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <thenewworld/Camera.hpp>

#include <string>

namespace thenewworld {

class FlyingText {
public:
    FlyingText();
    ~FlyingText();

    void start_animation(std::string text, unsigned animation_duration, unsigned fade_time);
    void update(int dt);
    void render(Camera&);

    enum class AnimationState { Idle,
        Entering,
        Displaying,
        Exiting };

private:
    std::string m_current_text;
    double m_text_height = 80;
    unsigned m_animation_progress,
        m_animation_duration,
        m_fade_time;
    AnimationState m_animation_state = AnimationState::Idle;
};

} // thenewworld
