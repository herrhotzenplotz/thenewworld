/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <thenewworld/GameState.hpp>
#include <thenewworld/Slideboard.hpp>
#include <thenewworld/SongConfig.hpp>

namespace thenewworld {

enum class GameRunningAnimationState { Idle,
    Finishing };

enum class GameLoadMode { FromFile,
    FromMemoryConfig };

class GameRunningState : public GameState {
public:
    GameRunningState(std::string path);
    GameRunningState(SongConfig*);
    GameRunningState(GameRunningState&&);

    virtual ~GameRunningState();

    virtual void init(GameContext&) override;
    virtual void event(GameContext&, SDL_Event&) override;
    virtual void update(GameContext&, int) override;
    virtual void render(Camera&) override;

private:
    Slideboard* m_board = nullptr;
    SongConfig* m_songs = nullptr;
    std::string m_config_path;
    GameRunningAnimationState m_animation_state = GameRunningAnimationState::Idle;
    int m_animation_progress = 0,
        m_animation_duration = 100;
    GameLoadMode m_load_mode;
};

} // thenewworld
