/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <thenewworld/GameState.hpp>
#include <thenewworld/Menu.tpp>

namespace thenewworld {

class LevelSelectorState : public GameState {
public:
    LevelSelectorState();
    virtual ~LevelSelectorState() {}
    virtual void init(GameContext&) override;
    virtual void render(Camera&) override;
    virtual void event(GameContext&, SDL_Event&) override;
    virtual void update(GameContext&, int) override;

    enum class EntryType { File,
        Abort,
        Random };

    enum class AnimationState { Starting,
        Idle,
        Aborting,
        Done };

    struct FileMenuEntry : public MenuEntry<EntryType> {
    public:
        FileMenuEntry() = default;
        FileMenuEntry(const FileMenuEntry&) = default;
        FileMenuEntry(const std::string title, const std::string set_path);
        ~FileMenuEntry() = default;
        const std::string& set_path() const
        {
            return m_set_path;
        }

    private:
        const std::string m_set_path;
    };

    struct RandomGameMenuEntry : public MenuEntry<EntryType> {
        RandomGameMenuEntry();
        RandomGameMenuEntry(const RandomGameMenuEntry&) = default;
        ~RandomGameMenuEntry() = default;
    };

    struct AbortMenuEntry : public MenuEntry<EntryType> {
        AbortMenuEntry();
        AbortMenuEntry(const AbortMenuEntry&) = default;
        ~AbortMenuEntry() = default;
    };

private:
    Menu<EntryType> m_menu;
    AnimationState m_animation_state = AnimationState::Starting;
    int m_animation_duration = 400,
        m_animation_progress = 0;
    void select_item(GameContext&);
};

} // thenewworld
