/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <thenewworld/Camera.hpp>
#include <thenewworld/GameState.hpp>
#include <thenewworld/Slideboard.hpp>

#include <atomic>
#include <random>
#include <thread>

namespace thenewworld {

enum class GeneratingAnimationState { Starting,
    Idle,
    Finish };

class GeneratingRandomLevelState : public GameState {
public:
    GeneratingRandomLevelState(unsigned cols, unsigned rows);
    virtual ~GeneratingRandomLevelState();
    virtual void render(Camera&) override;
    virtual void update(GameContext&, int) override;
    virtual void init(GameContext&) override;

    unsigned width() { return m_cols; }
    unsigned height() { return m_rows; }

private:
    enum class GeneratorThreadResult {
        Success,
        Failed,
        Running
    };
    struct GeneratorThreadState {
        std::atomic<GeneratorThreadResult> thread_state { GeneratorThreadResult::Running };
        GeneratingRandomLevelState* level_state;
        std::string error_message;
        std::mt19937* prng;
        SongConfig* config;
    };

    static void generate_level(GeneratorThreadState*);

    unsigned m_cols,
        m_rows;

    GeneratorThreadState m_thread_state;
    std::thread m_generator_thread;
    GeneratingAnimationState m_animation_state { GeneratingAnimationState::Starting };
    int m_animation_progress = 0,
        m_animation_duration = 400;
};

} // thenewworld
