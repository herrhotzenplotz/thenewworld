/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <thenewworld/Color.hpp>
#include <thenewworld/FlyingText.hpp>
#include <thenewworld/GameState.hpp>
#include <thenewworld/Rect.hpp>

namespace thenewworld {
class ArkanoidGameState : public GameState {
public:
    ArkanoidGameState(Color col, int velocity, double angle, double gravity, double friction);
    ~ArkanoidGameState()
    {
        if (!m_image)
            delete m_image;
    }

    virtual void init(GameContext&) override;
    virtual void event(GameContext&, SDL_Event&) override;
    virtual void update(GameContext&, int) override;
    virtual void render(Camera&) override;

private:
    double m_vel_x;
    double m_vel_y;
    double m_gravity;
    double m_friction;
    Rect m_rect { 180, 180, 0, 0 };
    Color m_col_rect;
    SDLImage* m_image;
    FlyingText m_fly;
};
} // thenewworld
