/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once
#include <thenewworld/Color.hpp>
#include <thenewworld/GameState.hpp>
#include <thenewworld/Menu.tpp>
#include <thenewworld/Rect.hpp>
#include <thenewworld/SoundSample.hpp>
#include <thread>
#include <vector>

namespace thenewworld {

class MenuState : public GameState {
public:
    MenuState();
    ~MenuState();
    virtual void init(GameContext&) override;
    virtual void event(GameContext&, SDL_Event&) override;
    virtual void update(GameContext&, int) override;
    virtual void render(Camera&) override;
    void set_come_from_random_menu()
    {
        m_anim_state = MenuAnimationState::ComeFromSubMenu;
        m_animation_duration = 300;
    }

    enum class MenuStateEntryType { Quit,
        About,
        Singleplayer,
        Multiplayer
    };

    enum class MenuAnimationState { StartingMenu,
        Idle,
        QuittingGame,
        StartingAboutPage,
        StartingSubMenu,
        ComeFromSubMenu };

    enum SubmenuRequestType { Singleplayer,
        Multiplayer };

    enum class AnimationDirection { Forward,
        Backward };

    struct MultiplayerMenuEntry : public MenuEntry<MenuStateEntryType> {
        MultiplayerMenuEntry();
        MultiplayerMenuEntry(const MultiplayerMenuEntry&) = default;
        ~MultiplayerMenuEntry() = default;
    };

    struct SingleplayerMenuEntry : public MenuEntry<MenuStateEntryType> {
        SingleplayerMenuEntry();
        SingleplayerMenuEntry(const SingleplayerMenuEntry&) = default;
        ~SingleplayerMenuEntry() = default;
    };

    struct QuitMenuEntry : public MenuEntry<MenuStateEntryType> {
        QuitMenuEntry();
        QuitMenuEntry(const QuitMenuEntry&) = default;
        ~QuitMenuEntry() = default;
    };

    struct AboutPageMenuEntry : public MenuEntry<MenuStateEntryType> {
        AboutPageMenuEntry();
        AboutPageMenuEntry(const AboutPageMenuEntry&) = default;
        ~AboutPageMenuEntry() = default;
    };

private:
    void render_idle(Camera&);
    void render_start_menu(Camera&);
    void render_start_game(Camera&);
    void render_quit_game(Camera&);
    void render_sub_menu(Camera&, AnimationDirection);
    double render_title_and_get_entries_y0(Camera&, const Color&);
    bool quit_request();
    bool single_game_request();
    bool multi_game_request();
    bool about_page_request();

    Menu<MenuStateEntryType> m_menu;
    MenuAnimationState m_anim_state = MenuAnimationState::StartingMenu;
    int m_animation_duration = 800,
        m_animation_progress = 0;
    SubmenuRequestType m_submenu_request;
};
} // thenewworld
