/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#pragma once

#include <thenewworld/GameState.hpp>
#include <thenewworld/Rect.hpp>

namespace thenewworld {
class ErrorState : public GameState {
public:
    ErrorState(std::string message);

    template<typename F>
    ErrorState(std::string message, F error_getter)
        : m_message(message)
    {
        m_message.append({ error_getter() });
    }

    ErrorState(const ErrorState&) = default;
    virtual ~ErrorState() { }

    virtual void init(GameContext&) override;
    virtual void event(GameContext&, SDL_Event&) override;
    virtual void update(GameContext&, int) override;
    virtual void render(Camera&) override;

private:
    void print_line(Camera&, std::string);

    std::string m_message;
    double m_y0 = 50.0;
    const double m_x0 = 50.0;
    const double m_y_margin = 50.0;
    const double m_padding = 10;
    double m_text_height = 0.0;
    Rect m_bounce_rect = { 30, 30, 0, 0 };
    double m_gravity = 10.0,
           m_velocity = 0.0,
           m_drag = 0.4,
           m_velocity_kick = -110;
    int m_counter = 0, m_reset_point = 1700;
};
} // thenewworld
