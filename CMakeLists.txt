cmake_minimum_required(VERSION 2.8.3)
project(thenewworld)

add_compile_options(-std=gnu++17 -Wall -Wextra -Werror -fno-builtin -pedantic -pedantic-errors -ggdb -O2 -DLANG_DE)
find_package(PkgConfig REQUIRED)
find_package(Boost REQUIRED COMPONENTS system filesystem iostreams)
find_package(Threads)
find_package(Curses REQUIRED)
find_package(SDL2 REQUIRED sdl2)

pkg_search_module(SDL2IMAGE REQUIRED SDL2_image)
pkg_search_module(SDL2TTF REQUIRED SDL2_ttf)
pkg_search_module(YAML REQUIRED yaml-cpp)

macro(compile_asset srcfile destfile)
  message(STATUS "Compiling asset ${srcfile}")
  execute_process(COMMAND bash -c "xxd -i ../${srcfile} > ${CMAKE_CURRENT_BINARY_DIR}/${destfile}")
endmacro(compile_asset)

include_directories(
  include/
  ${CMAKE_CURRENT_BINARY_DIR}/
  thirdparty/
  ${Boost_INCLUDE_DIR}
  ${SDL2_INCLUDE_DIRS}
  ${SDL2IMAGE_INCLUDE_DIRS}
  ${SDL2TTF_INCLUDE_DIRS}
  ${CURSES_INCLUDE_DIR}
)

add_executable(${PROJECT_NAME}
  src/SDLAudio.cpp
  src/GameContext.cpp
  src/SDLImage.cpp
  src/Slide.cpp
  src/Slideboard.cpp
  src/SongConfig.cpp
  src/Color.cpp
  src/SDLWindow.cpp
  src/Camera.cpp
  src/FlyingText.cpp
  src/SlideMatchedInfoRenderer.cpp
  src/States/ArkanoidGameState.cpp
  src/States/AboutPageState.cpp
  src/States/ErrorState.cpp
  src/States/GameRunningState.cpp
  src/States/MenuState.cpp
  src/States/GeneratingRandomLevelState.cpp
  src/States/MultiplayerRunningState.cpp
  src/States/RandomMenuState.cpp
  src/States/LevelSelectorMenuState.cpp
  src/States/GameOverState.cpp
  src/Rect.cpp
  src/level_editor.cpp
  src/main.cpp
)

target_link_libraries(${PROJECT_NAME} LINK_PUBLIC
  ${CMAKE_THREAD_LIBS_INIT}
  ${Boost_LIBRARIES}
  ${SDL2_LIBRARIES}
  ${SDL2IMAGE_LIBRARIES}
  ${SDL2TTF_LIBRARIES}
  ${YAML_LIBRARIES}
  ${CURSES_LIBRARIES}
  )

file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/data DESTINATION ${CMAKE_CURRENT_BINARY_DIR})

compile_asset(assets/intro_ambient.mp3 intro_music.h)
compile_asset(assets/notes.png notes_png.h)
compile_asset(assets/accidentials.png accidentials_png.h)
compile_asset(assets/clef_bass.png clef_bass_png.h)
compile_asset(assets/clef_treble.png clef_treble_png.h)
compile_asset(Feedjique.ttf feedjique.h)
compile_asset(assets/placeholder.png placeholder_png.h)
