/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <thenewworld/GameContext.hpp>
#include <thenewworld/SDLInitializer.hpp>

#include <boost/filesystem.hpp>

using namespace thenewworld;

int level_editor_main(void);

void check_data_folder()
{
    if (!boost::filesystem::exists("data/")) {
        log_info("data/ directory doesn't exist. Creating it.");
        boost::filesystem::create_directory(boost::filesystem::current_path().append("data"));
    }
}

int main(int argc, char** args)
{
    check_data_folder();

    if (argc == 2 && (strcmp(args[1], "editor") == 0)) {
        return level_editor_main();
    } else {
        SDLInitializer iniializer {};
        GameContext game { !(argc == 2 && (strcmp(args[1], "--no-force-opengl") == 0)) };
        return game.run();
    }
}
