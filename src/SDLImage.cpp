/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <boost/filesystem.hpp>
#include <thenewworld/Logging.hpp>
#include <thenewworld/SDLImage.hpp>

namespace fs = boost::filesystem;

namespace thenewworld {

SDLImage::SDLImage(std::string path, SDL_Renderer* renderer)
{
    log_info("Loading image from '" + path + "'");

    fs::path filepath { path };

    enforce_lambda([](void) -> std::string { return "No such file or directory."; },
        fs::exists(filepath) && fs::is_regular_file(filepath),
        "Cannot open file " + path);

    auto* img_surf = IMG_Load(path.c_str());
    enforce_typed(IMG_GetError,
        img_surf,
        "Could not load image to surface");

    m_image_texture = SDL_CreateTextureFromSurface(renderer, img_surf);
    enforce(m_image_texture, "SDL_CreateTextureFromSurface");
    SDL_FreeSurface(img_surf);

    enforce(SDL_SetTextureBlendMode(m_image_texture, SDL_BLENDMODE_BLEND) == 0,
        "SDL_SetTextureBlendMode");

    m_rect = SDL_Rect { 0, 0,
        img_surf->w,
        img_surf->h };
}

SDLImage::SDLImage(void* buffer, size_t buffer_size, SDL_Renderer* renderer)
{
    log_info("Loading image from memory buffer");

    auto* img_surf = IMG_Load_RW(SDL_RWFromMem(buffer, (int)buffer_size), 1);
    enforce_typed(IMG_GetError,
        img_surf,
        "Could not load image from buffer to surface");

    m_image_texture = SDL_CreateTextureFromSurface(renderer, img_surf);
    enforce(m_image_texture, "SDL_CreateTextureFromSurface");
    SDL_FreeSurface(img_surf);

    enforce(SDL_SetTextureBlendMode(m_image_texture, SDL_BLENDMODE_BLEND) == 0,
        "SDL_SetTextureBlendMode");

    m_rect = SDL_Rect { 0, 0,
        img_surf->w,
        img_surf->h };
}

SDLImage::SDLImage(SDLImage&& other)
{
    log_info("Moving SDLImage");
    memmove(&m_rect, other.rect(), sizeof(SDL_Rect));
    m_image_texture = other.texture();
}

SDLImage::~SDLImage()
{
    log_info("Destroying SDLImage");
    SDL_DestroyTexture(m_image_texture);
}

} // thenewworld
