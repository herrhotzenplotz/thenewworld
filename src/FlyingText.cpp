/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <thenewworld/FlyingText.hpp>
#include <thenewworld/Logging.hpp>

namespace thenewworld {

FlyingText::FlyingText()
{
}

FlyingText::~FlyingText()
{
}

void FlyingText::start_animation(std::string text, unsigned animation_duration, unsigned fade_time)
{
    enforce_msg(animation_duration > 2 * fade_time,
        "The total animation duration must be greater than twice the fade time");
    m_animation_progress = 0;
    m_animation_duration = animation_duration;
    m_fade_time = fade_time;
    m_current_text = text;
    m_animation_state = AnimationState::Entering;
}

void FlyingText::update(int dt)
{
    if (m_animation_state == AnimationState::Idle)
        return;

    /* Here we apply the following procedure:
     *
     * 1. Update the timestamp with dt
     *
     * 2. Check the boundaries of the animation and set the current
     *   state accordingly.
     */
    m_animation_progress += dt;
    if (m_animation_progress < m_fade_time) {
        m_animation_state = AnimationState::Entering;
    } else if (m_animation_duration - m_fade_time >= m_animation_progress) {
        m_animation_state = AnimationState::Displaying;
    } else {
        if (m_animation_progress >= m_animation_duration) {
            m_animation_state = AnimationState::Idle;
            return;
        }

        m_animation_state = AnimationState::Exiting;
    }
}

void FlyingText::render(Camera& cam)
{
    auto vp = cam.get_viewport();
    auto text_rect = cam.text_boundaries(m_current_text);
    text_rect.scale_by(m_text_height / text_rect.h);

    // now center the mothaflippa on the y axis.

    text_rect.y = (vp.h - text_rect.h) * 0.5;

    // x position is done here depending on the animation state
    switch (m_animation_state) {
    case AnimationState::Idle:
        return;
    case AnimationState::Entering: {
        text_rect.x = (vp.w - text_rect.w) * (1.5 - (float)m_animation_progress / (float)m_fade_time);
    } break;
    case AnimationState::Displaying: {
        text_rect.x = (vp.w - text_rect.w) * 0.5;
    } break;
    case AnimationState::Exiting: {
        text_rect.x = (vp.w - text_rect.w) * (0.5 - 2 * (1.0F - (float)(m_animation_duration - m_animation_progress) / (float)m_fade_time));
    } break;
    }

    cam.render_text(m_current_text, Color::white, text_rect);
}

} // thenewworld
