/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <boost/filesystem.hpp>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <iterator>
#include <vector>

#ifdef __FreeBSD__
#include <curses.h>
#elif
#include <ncurses/curses.h>
#endif

#include <yaml-cpp/yaml.h>

#include <thenewworld/Logging.hpp>
#include <thenewworld/SDLAudio.hpp>
#include <thenewworld/SongConfig.hpp>
#include <thenewworld/translation.hpp>

#include <SDL2/SDL.h>

#define STYLE_SELECTED 1
#define STYLE_UNSELECTED 2

static thenewworld::SDLAudioPlayer* leveleditor_player = nullptr;

void bold(const std::string& s)
{
    attron(A_BOLD | COLOR_PAIR(STYLE_SELECTED));
    addstr(s.c_str());
    attroff(A_BOLD | COLOR_PAIR(STYLE_SELECTED));
}

void underline(const std::string& s)
{
    attron(A_UNDERLINE | COLOR_PAIR(STYLE_UNSELECTED));
    addstr(s.c_str());
    attroff(A_UNDERLINE | COLOR_PAIR(STYLE_UNSELECTED));
}

void normal(const std::string& s)
{
    addstr(s.c_str());
}

struct MainMenuEntry {
    enum class Type { New,
        Open,
        Quit };

    std::string title;
    std::string filename;
    Type type;
};

void draw_footer(std::string text)
{
    int rows, cols;
    getmaxyx(stdscr, rows, cols);
    move(rows - 1, 0);
    clrtoeol();
    move(rows - 1, (cols - text.length()) / 2);
    normal(text);
}

void draw_header(std::string header)
{
    int rows, cols;
    (void)rows;
    getmaxyx(stdscr, rows, cols);
    move(3, (cols - header.length()) / 2);
    underline(header);
}

std::string file_browser()
{
    std::string current_dir = "data/";
    int selected_idx = 0;

    while (true) {
        int rows, cols, size = 0;
        std::string matched_file;
        (void)rows;

        getmaxyx(stdscr, rows, cols);
        erase();
        draw_header(T_LE_FileBrowser);
        for (auto& file : boost::filesystem::directory_iterator(current_dir)) {

            // Skip unsupported files
            auto extension = file.path().extension();
            if (!(extension == ".mp3" || extension == ".wav"))
                continue;

            auto name = file.path().filename().string();
            move(7 + size, (cols - name.length()) / 2);
            if (size == selected_idx) {
                matched_file = name;
                bold(name);
            } else {
                normal(name);
            }

            size += 1;
        }

        draw_footer(T_LE_FileBrowser_Footer);

        switch (getch()) {
        case KEY_UP:
        case 'w':
            selected_idx = std::max(0, std::min(selected_idx - 1, size - 1));
            break;
        case KEY_DOWN:
        case 's':
            selected_idx = std::max(0, std::min(selected_idx + 1, size - 1));
            break;
        case '\n':
            return current_dir + matched_file;
        case 'q':
            return "";
        }
    }
}

int run_main_menu(std::vector<MainMenuEntry>& entries)
{
    int selected_idx = 0,
        cols, rows;

    init_pair(STYLE_UNSELECTED, COLOR_WHITE, COLOR_BLACK);
    init_pair(STYLE_SELECTED, COLOR_BLACK, COLOR_WHITE);

    while (true) {
        getmaxyx(stdscr, rows, cols);
        int y0 = (rows - entries.size()) / 2;

        erase();
        draw_header(T_LE_Header);

        for (size_t i = 0; i < entries.size(); ++i) {
            move(y0 + i, (cols - entries[i].title.length()) / 2);
            if (selected_idx == (int)i) {
                bold(entries[i].title);
            } else {
                normal(entries[i].title);
            }
        }

        draw_footer(T_LE_Footer);
        switch (getch()) {
        case KEY_DOWN:
        case 's':
            selected_idx = std::min(selected_idx + 1, (int)entries.size() - 1);
            break;
        case KEY_UP:
        case 'w':
            selected_idx = std::max(selected_idx - 1, 0);
            break;
        case KEY_ENTER:
        case '\n':
            return selected_idx;
        case 'q':
            endwin();
            exit(EXIT_SUCCESS);
            break;
        default:
            break;
        }
    }
}

std::string* get_input(const char* prompt)
{
    char c;
    const std::string msg = T_LE_ConfOK;
    auto* input = new std::string;
    int rows, cols;
    getmaxyx(stdscr, rows, cols);

    do {
        endwin();
        std::cout << prompt;
        std::getline(std::cin, *input);
        erase();
        move(rows / 2, (cols - input->length()) / 2);
        addstr(input->c_str());
        move(rows / 2 + 1, (cols - msg.length()) / 2);
        normal(msg);
        c = getch();
    } while (!(c == 'y' || c == 'a'));

    if (c == 'y') {
        return input;
    } else {
        return nullptr;
    }
}

void edit_samples(std::array<thenewworld::OffsetSample, 2>& samples, std::string path)
{
    int rows, cols, mid, selected_idx = 0;
    const std::string label_begin = T_LE_SampleBegin,
                      label_duration = T_LE_SampleDuration;
    (void)rows;
    getmaxyx(stdscr, rows, cols);
    mid = cols / 2;

    const auto& print_timestamp = [](int secs) {
        int m_hours = (int)(secs / 3600);
        int m_minutes = (int)((secs % 3600) / 60);
        int m_secs = (int)(secs - ((m_hours * 60 + m_minutes) * 60));

        normal(std::to_string(m_hours) + ":" + std::to_string(m_minutes) + ":" + std::to_string(m_secs));
    };

    while (true) {
        erase();

        draw_header(T_LE_SampleEditor);

        int selected_sample = selected_idx / 2,
            selected_field = selected_idx % 2;

        for (int i = 1; i <= (int)samples.size(); ++i) {
            std::string sample_header = T_LE_SampleNo + std::to_string(i);
            move(5 * i + 2, mid - sample_header.length());
            underline(sample_header);

            move(5 * i + 4, mid - label_begin.length());
            if (((selected_sample + 1) == i) && (selected_field == 0))
                bold(label_begin);
            else
                normal(label_begin);

            move(5 * i + 4, mid + 1);
            print_timestamp((int)samples[i - 1].start);

            move((5 * i) + 5, mid - label_duration.length());
            if (((selected_sample + 1) == i) && (selected_field == 1))
                bold(label_duration);
            else
                normal(label_duration);
            move(5 * i + 5, mid + 1);
            print_timestamp((int)samples[i - 1].len);
        }

        draw_footer(T_LE_SampleEditorFooter);

        switch (getch()) {
        case 'q':
            return;
        case KEY_RIGHT:
        case 'i': {
            if (selected_field == 0)
                samples[selected_sample].start += 1.0;
            else
                samples[selected_sample].len += 1.0;
        } break;
        case KEY_LEFT:
        case 'k': {
            if (selected_field == 0)
                samples[selected_sample].start -= 1.0;
            else
                samples[selected_sample].len -= 1.0;
        } break;
        case KEY_UP:
        case 'w':
            selected_idx = std::max(0, std::min(selected_idx - 1, ((int)samples.size() * 2) - 1));
            break;
        case KEY_DOWN:
        case 's':
            selected_idx = std::max(0, std::min(selected_idx + 1, ((int)samples.size() * 2) - 1));
            break;
        case 'p': {
            // TODO(#51): Automatically return to edit mode after playback finished playing
            if (!boost::filesystem::exists(path)) {
                draw_footer(T_LE_SampleNotExists);
                getch();
                break;
            }

            auto* loaded_sample = leveleditor_player->load_mp3_file(path, samples);
            if (!loaded_sample) {
                draw_footer(T_LE_CannotSeek);
                getch();
                break;
            }

            leveleditor_player->play_sample(&loaded_sample[selected_sample]);
            draw_footer(T_LE_EndPlayback);
            getch();
            leveleditor_player->stop_playback();
            delete[] loaded_sample;
        } break;
        default:
            break;
        }
    }
}

void edit_song(thenewworld::Song& song)
{
    enum class SongProperty { Filename = 0,
        Title,
        Movement,
        Composer,
        Samples,
        N };

    const std::array<std::string, (size_t)SongProperty::N> captions = T_LE_SongProperties;
    int rows, cols, selected_idx = 0;
    (void)rows;

    while (true) {
        getmaxyx(stdscr, rows, cols);

        erase();
        draw_header(T_LE_SongEditor);
        draw_footer(T_LE_SongEditor_Footer);

        // Render the options we can edit
        int mid = cols / 4,
            y0 = 6;

        for (int i = 0; i < (int)captions.size(); ++i) {
            move(y0 + i, mid - captions[i].length() - 1);

            if (i == selected_idx) {
                bold(captions[i]);
            } else {
                normal(captions[i]);
            }

            move(y0 + i, mid + 1);

            switch ((SongProperty)i) {
            case SongProperty::Filename:
                normal(song.filepath);
                break;
            case SongProperty::Title:
                normal(song.title);
                break;
            case SongProperty::Movement:
                normal(song.movement);
                break;
            case SongProperty::Composer:
                normal(song.composer);
                break;
            case SongProperty::Samples:
                normal(T_LE_EnterToEdit);
            default:
                break;
            }
        }

        switch (getch()) {
        case 'q':
            return;
        case '\n': {
            if ((SongProperty)selected_idx == SongProperty::Samples) {
                edit_samples(song.offset_samples, song.filepath);
            } else if ((SongProperty)selected_idx == SongProperty::Filename) {
                auto new_path = file_browser();
                if (new_path.empty())
                    break;
                else
                    song.filepath = new_path;
            } else {
                const std::string* input = get_input(captions[selected_idx].c_str());
                if (input != nullptr) {
                    switch ((SongProperty)selected_idx) {
                    case SongProperty::Title:
                        song.title = *input;
                        break;
                    case SongProperty::Movement:
                        song.movement = *input;
                        break;
                    case SongProperty::Composer:
                        song.composer = *input;
                        break;
                    default:
                        break;
                    }
                }
            }
        } break;
        case KEY_UP:
        case 'w': {
            selected_idx = std::max(0, selected_idx - 1);
        } break;
        case KEY_DOWN:
        case 's': {
            selected_idx = std::min((int)SongProperty::N - 1, selected_idx + 1);
        } break;
        default:
            break;
        }
    }
}

void edit_file(MainMenuEntry& entry)
{
    std::string filename = entry.title;
    int rows, cols,
        filename_len = entry.title.length();
    thenewworld::SongConfig* config = nullptr;
    getmaxyx(stdscr, rows, cols);

    try {
        config = thenewworld::SongConfig::parse_unsafe(entry.filename);
    } catch (YAML::Exception&) {
        std::string msg = T_LE_CorruptedFile;
        move(rows / 2, (cols - msg.length()) / 2);
        normal(msg);
        getch();
        return;
    }

    int selected_idx = 0;
    while (true) {
        erase();
        draw_header(T_LE_EditLevel);

        move(4, (cols - filename_len) / 2);
        normal(filename);

        int y0 = 6;

        for (int i = 0; i < (int)config->songs.size(); ++i) {
            move(y0 + i, (cols - config->songs[i].title.length()) / 2);
            if (i == selected_idx) {
                bold(config->songs[i].title);
            } else {
                normal(config->songs[i].title);
            }
        }
        draw_footer(T_LE_EditLevel_Footer);

        switch (getch()) {
        case '\n':
            edit_song(config->songs[selected_idx]);
            break;
        case 'q':
            return;
        case KEY_UP:
        case 'w':
            selected_idx = std::max(0, selected_idx - 1);
            break;
        case KEY_DOWN:
        case 's':
            selected_idx = std::min(selected_idx + 1, (int)config->songs.size() - 1);
            break;
        case 'n':
            config->songs.push_back(thenewworld::Song { "", "New Song", "Movement 1", "No composer", 0, {}, {}, {} });
            break;
        case 'k': {
            char c;
            do {
                move(rows - 1, 0);
                normal(T_LE_Sure);
                c = getch();
            } while (c != 'y' && c != 'n');

            if (c == 'y') {
                config->songs.erase(config->songs.begin() + selected_idx);
            }
        } break;
        case 'x': {
            char c;
            do {
                move(rows - 1, 0);
                normal(T_LE_Sure);
                c = getch();
            } while (c != 'y' && c != 'n');

            if (c == 'n')
                break;

            YAML::Emitter out;
            const auto& timestamp = [](int secs) -> std::string {
                int m_hours = (int)(secs / 3600);
                int m_minutes = (int)((secs % 3600) / 60);
                int m_secs = (int)(secs - ((m_hours * 60 + m_minutes) * 60));

                return std::to_string(m_hours) + ":" + std::to_string(m_minutes) + ":" + std::to_string(m_secs) + ".0";
            };

            out << YAML::BeginSeq;

            for (auto& song : config->songs) {
                out << YAML::BeginMap;
                out << YAML::Key << "file" << YAML::Value << song.filepath;
                out << YAML::Key << "title" << YAML::Value << song.title;
                if (!song.movement.empty()) {
                    out << YAML::Key << "movement" << YAML::Value << song.movement;
                }
                out << YAML::Key << "composer" << YAML::Value << song.composer;
                out << YAML::Key << "samples" << YAML::Value << YAML::BeginSeq;

                for (auto& sample : song.offset_samples) {
                    out << YAML::BeginMap;
                    out << YAML::Key << "start" << YAML::Value << timestamp(sample.start);
                    out << YAML::Key << "end" << YAML::Value << timestamp(sample.start + sample.len);
                    out << YAML::EndMap;
                }
                out << YAML::EndSeq;

                out << YAML::EndMap;
            }

            out << YAML::EndSeq;

            auto* f = fopen(entry.filename.c_str(), "w");
            if (f == nullptr) {
                fprintf(stderr, "FAIL : Couldn't open file for emitting yaml.\n");
                abort();
            }

            fwrite(out.c_str(), sizeof(char), out.size(), f);
            fclose(f);
        } break;
        }
    }
}

int level_editor_main(void)
{
    using namespace thenewworld;
    enforce(SDL_Init(SDL_INIT_AUDIO) == 0, "SDL_Init");
    leveleditor_player = new SDLAudioPlayer;

    initscr();
    keypad(stdscr, true);

    std::vector<MainMenuEntry> entries
        = {
              { T_LE_NewLevel, "", MainMenuEntry::Type::New },
          };

    for (auto& path : boost::filesystem::directory_iterator("data/")) {
        if (boost::filesystem::is_regular_file(path) && path.path().extension() == ".yaml") {
            entries.push_back({ path.path().stem().string(),
                path.path().string(),
                MainMenuEntry::Type::Open });
        }
    }

    entries.push_back({ T_LE_Quit, "", MainMenuEntry::Type::Quit });
    while (true) {
        int selected = run_main_menu(entries);

        switch (entries[selected].type) {
        case MainMenuEntry::Type::Quit:
            endwin();
            return EXIT_SUCCESS;
        case MainMenuEntry::Type::New: {
            endwin();
            auto* inp = get_input(T_LE_LevelName);
            if (inp == nullptr)
                break;

            auto file_name = "data/" + *inp + ".yaml";
            entries.push_back(MainMenuEntry { *inp, file_name, MainMenuEntry::Type::Open });
            std::ofstream file(file_name);
            file.close();

            edit_file(entries.back());

        } break;
        case MainMenuEntry::Type::Open:
            edit_file(entries[selected]);
            break;
        }
    }

    delete leveleditor_player;
    SDL_Quit();
    return EXIT_SUCCESS;
}
