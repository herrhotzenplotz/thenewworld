/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <SDL2/SDL.h>
#include <SDL2/SDL_ttf.h>
#include <thenewworld/Camera.hpp>
#include <thenewworld/Color.hpp>
#include <thenewworld/Logging.hpp>
#include <thenewworld/Vec2.hpp>

#include <feedjique.h>

namespace thenewworld {
Camera::Camera(SDL_Window* window, bool force_opengl)
{

    int num_drivers = SDL_GetNumRenderDrivers();
    enforce(num_drivers > 0, "SDL_GetNumRenderDrivers");

    log_info("Creating Renderer for SDL Window");
    SDL_RendererInfo info;
    if (force_opengl) {
        for (int i = 0; i < num_drivers; ++i) {
            SDL_GetRenderDriverInfo(i, &info);
            if (strcmp(info.name, "opengl") == 0) {
                create_renderer_with_id(window, i);
                break;
            }
        }
    } else {
        create_renderer_with_id(window, -1);
    }

    enforce(m_renderer != nullptr,
        "Could not create renderer. This is either because thenewworld is "
        "forcing an OpenGL rendering driver or no suitable driver has "
        "been found by SDL2. If you do not want this behaviour, "
        "please start the game with the --no-force-opengl flag.");

    enforce(SDL_GetRendererInfo(m_renderer, &info) == 0,
        "SDL_GetRendererInfo");
    log_info("Using render driver '" + std::string(info.name) + "'");

    enforce(SDL_SetRenderDrawBlendMode(m_renderer, SDL_BLENDMODE_BLEND) == 0,
        "SDL_SetRenderDrawBlendMode");

    log_info("Loading embedded fonts");

    for (auto& font_entry : m_fonts) {
        font_entry.second = TTF_OpenFontRW(SDL_RWFromMem(___Feedjique_ttf,
                                               (int)___Feedjique_ttf_len),
            1, font_entry.first);
        enforce_typed(TTF_GetError,
            font_entry.second,
            "Could not load font");
    }
}

void Camera::create_renderer_with_id(SDL_Window* window, int id)
{
    m_renderer = SDL_CreateRenderer(window, id,
        SDL_RENDERER_ACCELERATED
            | SDL_RENDERER_PRESENTVSYNC);
}

Camera::~Camera()
{
    log_info("Unloading fonts");
    for (auto& pair : m_fonts) {
        TTF_CloseFont(pair.second);
    }

    log_info("Destroying Renderer");
    SDL_DestroyRenderer(m_renderer);
}

Rect Camera::get_viewport() const
{
    SDL_Rect viewport;
    SDL_RenderGetViewport(m_renderer, &viewport);
    return { viewport };
}

void Camera::fill_screen(const Color col)
{
    const auto sdl_col = to_sdl_color(col);
    enforce(SDL_SetRenderDrawColor(m_renderer,
                sdl_col.r,
                sdl_col.g,
                sdl_col.b,
                sdl_col.a)
            >= 0,
        "SDL_SetRenderDrawColor");
    enforce(SDL_RenderClear(m_renderer) >= 0,
        "SDL_RenderClear");
}

void Camera::clear_screen()
{
    fill_screen(Color::black);
}

void Camera::present() const
{
    SDL_RenderPresent(m_renderer);
}

int Camera::render_width() const
{
    int w = 0;
    enforce(SDL_GetRendererOutputSize(m_renderer, &w, nullptr) >= 0,
        "SDL_GetRendererOutputSize");
    return w;
}

void Camera::draw_filled_rect(const Rect rect, const Color color)
{
    const auto sdl_rect = to_sdl_rect(rect);
    const auto sdl_col = to_sdl_color(color);
    enforce(SDL_SetRenderDrawColor(m_renderer,
                sdl_col.r,
                sdl_col.g,
                sdl_col.b,
                sdl_col.a)
            >= 0,
        "SDL_SetRenderDrawColor");

    enforce(SDL_RenderFillRect(m_renderer, &sdl_rect) >= 0,
        "SDL_RenderFillRect");
}

void Camera::draw_rect(const Rect rect, const Color color)
{
    const auto sdl_rect = to_sdl_rect(rect);
    const auto sdl_col = to_sdl_color(color);
    enforce(SDL_SetRenderDrawColor(m_renderer,
                sdl_col.r,
                sdl_col.g,
                sdl_col.b,
                sdl_col.a)
            >= 0,
        "SDL_SetRenderDrawColor");
    SDL_Point points[5] = {
        { sdl_rect.x, sdl_rect.y },
        { sdl_rect.x + sdl_rect.w, sdl_rect.y },
        { sdl_rect.x + sdl_rect.w, sdl_rect.y + sdl_rect.h },
        { sdl_rect.x, sdl_rect.y + sdl_rect.h },
        { sdl_rect.x, sdl_rect.y },
    };
    enforce(SDL_RenderDrawLines(m_renderer, points, 5) >= 0,
        "SDL_RenderFillRect");
}

Rect Camera::text_boundaries(const std::string& text) const
{
    int w, h;
    enforce_typed(TTF_GetError,
        !TTF_SizeUTF8(m_fonts.begin()->second, text.c_str(), &w, &h),
        "TTF_SizeUTF8");
    return { (double)w, (double)h, 0, 0 };
}

void Camera::render_text(std::string text, Color col, Rect dst_rect)
{
    // Find ideal font size. The fonts are in a map. That way we ensure that they
    // are stored in ascending font size. I just need to pick the first size that
    // is bigger than the height of the dst_rect.
    // I chose to do it like that to prevent a scale up of the font because that
    // looks ugly
    std::pair<double, TTF_Font*> best;
    for (auto& entry : m_fonts) {
        if (entry.first >= dst_rect.h) {
            best = entry;
            break;
        }
    }

    // FIXME(#54): The biggest available font size should not be hardcoded
    // It should be determined at runtime
    // ---
    // HACK! OMEGALUL!
    if (best.second == nullptr) {
        int highest = 300; // This is the highest key in the lookup table
        best.second = m_fonts[highest];
        best.first = highest;
    }

    auto* surf = TTF_RenderUTF8_Blended(best.second, text.c_str(), to_sdl_color(col));

    enforce_typed(TTF_GetError,
        surf,
        "TTF_RenderText_Blended");
    auto* texture = SDL_CreateTextureFromSurface(m_renderer, surf);
    enforce(texture, "SDL_CreateTextureFromSurface");

    enforce(SDL_SetTextureBlendMode(texture, SDL_BLENDMODE_BLEND) == 0,
        "SDL_SetTextureBlendMode");

    int w, h;
    enforce(SDL_QueryTexture(texture, nullptr, nullptr, &w, &h) == 0,
        "SDL_QueryTexture");

    auto srcrect = SDL_Rect { 0, 0, w, h };
    auto dstrect = to_sdl_rect(dst_rect);

    enforce(SDL_RenderCopy(m_renderer, texture, &srcrect, &dstrect) == 0,
        "SDL_RenderCopy");
    SDL_DestroyTexture(texture);
    SDL_FreeSurface(surf);
}

void Camera::render_image(SDLImage* img, Rect dst_rect)
{
    auto dstrect = to_sdl_rect(dst_rect);
    enforce(SDL_RenderCopy(m_renderer, img->texture(),
                img->rect(), &dstrect)
            == 0,
        "SDL_RenderCopy");
}

void Camera::push_translation(vec2 v)
{
    m_translation_stack.push_back(v);
}

void Camera::pop_translation()
{
    if (m_translation_stack.empty()) {
        log_warn("Attempting to pop from an empty translation stack");
    }

    m_translation_stack.pop_back();
}

SDLImage* Camera::create_image(std::string path)
{
    return new SDLImage { path, m_renderer };
}

SDLImage* Camera::create_image(void* buffer, size_t buffer_size)
{
    return new SDLImage { buffer, buffer_size, m_renderer };
}

vec2 Camera::translation() const
{
    vec2 total { 0, 0 };
    for (auto& v : m_translation_stack)
        total += v;
    return total;
}

const SDL_Rect Camera::to_sdl_rect(const Rect rect) const
{
    auto offset = translation();
    return { (int)round(rect.x + offset.x),
        (int)round(rect.y + offset.y),
        (int)round(rect.w),
        (int)round(rect.h) };
}

const SDL_Color Camera::to_sdl_color(const Color sys_col) const
{
    auto col = m_black_white_mode ? sys_col.desaturate() : sys_col;
    return {
        (uint8_t)round(col.r * 255.0),
        (uint8_t)round(col.g * 255.0),
        (uint8_t)round(col.b * 255.0),
        (uint8_t)round(col.a * 255.0)
    };
}
}
