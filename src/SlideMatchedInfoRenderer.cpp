/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <thenewworld/Configuration.hpp>
#include <thenewworld/SlideMatchedInfoRenderer.hpp>
#include <thenewworld/translation.hpp>

namespace thenewworld {

SlideMatchedInfoRenderer::SlideMatchedInfoRenderer(SongConfig* config)
    : m_songs(config)
{
}

void SlideMatchedInfoRenderer::update(int dt)
{
    switch (m_animation_state) {
    case InfoAnimationState::Opening: {
        if ((m_animation_progress += dt) >= m_animation_duration) {
            m_timer.start(INFO_DISPLAY_TIME);
            m_animation_state = InfoAnimationState::Showing;
        }
    } break;
    case InfoAnimationState::Closing: {
        if ((m_animation_progress += dt) >= m_animation_duration) {
            m_animation_state = InfoAnimationState::Idle;
        }
    } break;
    case InfoAnimationState::Showing: {
        if (m_timer.update(dt)) {
            m_animation_state = InfoAnimationState::Closing;
            m_animation_progress = 0;
        }
    } break;
    case InfoAnimationState::Idle: {
    } break;
    }
}

void SlideMatchedInfoRenderer::render(Camera& cam)
{
    auto vp = cam.get_viewport();
    auto text_rect = cam.text_boundaries(m_current_description);
    text_rect.scale_by(0.04 * vp.h / text_rect.h);
    text_rect.y = 0.0;
    text_rect.x = 0.5 * (vp.w - text_rect.w);

    switch (m_animation_state) {
    case InfoAnimationState::Opening: {
        cam.render_text(m_current_description,
            GAME_BACKGROUND_COLOR.interpolate(Color::white,
                m_animation_progress, m_animation_duration),
            text_rect);
    } break;
    case InfoAnimationState::Showing: {
        cam.render_text(m_current_description, Color::white, text_rect);
    } break;
    case InfoAnimationState::Closing: {
        cam.render_text(m_current_description,
            Color::white.interpolate(GAME_BACKGROUND_COLOR,
                m_animation_progress, m_animation_duration),
            text_rect);
    } break;
    default:
        break;
    }
}

void SlideMatchedInfoRenderer::show(int id)
{
    m_id = id;
    m_animation_state = InfoAnimationState::Opening;
    m_animation_progress = 0;
    m_current_description = T_GAME_THAT_WAS + m_songs->songs[(size_t)m_id].description();
}

} // thenewworld
