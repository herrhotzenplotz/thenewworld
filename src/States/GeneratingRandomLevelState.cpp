/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <algorithm>

#include <boost/filesystem.hpp>

#include <thenewworld/Configuration.hpp>
#include <thenewworld/GameContext.hpp>
#include <thenewworld/States/ErrorState.hpp>
#include <thenewworld/States/GameRunningState.hpp>
#include <thenewworld/States/GeneratingRandomLevelState.hpp>
#include <thenewworld/States/MenuState.hpp>
#include <thenewworld/translation.hpp>

namespace thenewworld {

GeneratingRandomLevelState::GeneratingRandomLevelState(unsigned cols, unsigned rows)
    : m_cols(cols)
    , m_rows(rows)
    , m_thread_state()
{
    m_thread_state.level_state = this;
}

GeneratingRandomLevelState::~GeneratingRandomLevelState()
{
    m_generator_thread.join();
}

void GeneratingRandomLevelState::init(GameContext& ctx)
{
    m_thread_state.prng = &ctx.prng();
    m_generator_thread = std::thread(generate_level, &m_thread_state);
}

void GeneratingRandomLevelState::render(Camera& cam)
{
    auto vp = cam.get_viewport();

    auto text_color = Color::white;
    auto screen_color = Color::black;
    if (m_animation_state == GeneratingAnimationState::Finish) {
        text_color = Color::white.interpolate(GAME_BACKGROUND_COLOR,
            m_animation_progress,
            m_animation_duration);
        screen_color = Color::black.interpolate(GAME_BACKGROUND_COLOR,
            m_animation_progress,
            m_animation_duration);
    } else if (m_animation_state == GeneratingAnimationState::Starting) {
        text_color = Color::black.interpolate(Color::white,
            m_animation_progress,
            m_animation_duration);
    }

    const std::string loading_message { T_RND_Digging };

    cam.fill_screen(screen_color);
    auto message_rect = cam.text_boundaries(loading_message);
    message_rect.scale_by(vp.w / message_rect.w * 0.6);
    message_rect.y = (vp.h - message_rect.h) * 0.5;
    message_rect.x = (vp.w - message_rect.w) * 0.5;

    cam.render_text(loading_message, text_color, message_rect);
}

void GeneratingRandomLevelState::update(GameContext& ctx, int dt)
{
    switch (m_animation_state) {
    case GeneratingAnimationState::Idle: {
        switch (m_thread_state.thread_state) {
        case GeneratorThreadResult::Success: {
            log_info("Generator thread is done");
            m_animation_state = GeneratingAnimationState::Finish;
            m_animation_progress = 0;
        } break;
        case GeneratorThreadResult::Failed: {
            ctx.set_state(std::make_unique<ErrorState>(m_thread_state.error_message));
        } break;
        default:
            break;
        }
    } break;
    case GeneratingAnimationState::Starting: {
        if ((m_animation_progress += dt) >= m_animation_duration) {
            m_animation_state = GeneratingAnimationState::Idle;
        }
    } break;
    case GeneratingAnimationState::Finish: {
        if ((m_animation_progress += dt) >= m_animation_duration) {
            ctx.set_state(std::make_unique<GameRunningState>(m_thread_state.config));
            ctx.stop_background_music();
        }
    } break;
    }
}

void GeneratingRandomLevelState::generate_level(GeneratorThreadState* state)
{
    state->config = SongConfig::empty();
    unsigned width = state->level_state->width(),
             height = state->level_state->height();
    const size_t distinct_file_cnt = (width * height) / 2;

    // Let's preemptively reserve some space
    state->config->songs.reserve(distinct_file_cnt);

    log_info("Searching for level definitions");

    // find all the config files and load them up into memory
    auto all_files = boost::filesystem::directory_iterator("data/");
    std::vector<std::unique_ptr<SongConfig>> configs;
    for (auto& path : all_files) {
        if (boost::filesystem::is_regular_file(path) && (path.path().extension() == ".yaml")) {
            SongConfig* loaded_config;
            try {
                loaded_config = SongConfig::parse_unsafe(path.path().string());
            } catch (YAML::Exception& e) {
                // blow up if failed to load config
                state->error_message = e.what();
                log_error(e.what());
                state->thread_state = GeneratorThreadResult::Failed;
                return;
            }

            configs.push_back(std::unique_ptr<SongConfig>(loaded_config));
        }
    }

    log_info("Done loading all definitions");
    size_t num_songs = 0;
    for (auto& sconfig : configs) {
        num_songs += sconfig->songs.size();
    }

    // Prevent an infinite loop that might occur later on when
    // choosing the songs for the level.
    if (num_songs < distinct_file_cnt) {
        state->error_message = "Less songs available than actually necessary for the board.";
        log_error("Less songs available than actually necessary for the board.");
        log_error("You need to have more songs in your configuration files to be able to "
                  "generate a board of demanded size.");
        state->thread_state = GeneratorThreadResult::Failed;
        return;
    }

    auto gen_rand = (*state->prng);
    // Calculates the song index of a level and the song idx within the level
    auto song_index = [&](size_t level_idx, size_t song_idx) {
        size_t result = 0;
        for (size_t i = 0; i < level_idx; ++i) {
            result += configs[i]->songs.size();
        }

        return result + song_idx;
    };

    log_info("Generating level");
    std::vector<int> selected_indicies(distinct_file_cnt, -1);
    for (size_t i = 0; i < distinct_file_cnt; ++i) {
        for (;;) {
            size_t level_idx = gen_rand() % configs.size();
            size_t song_idx = gen_rand() % (configs[level_idx]->songs.size());
            size_t selected_idx = song_index(level_idx, song_idx);
            if (std::count_if(
                    selected_indicies.begin(),
                    selected_indicies.end(),
                    [selected_idx](auto idx) {
                        return selected_idx == (size_t)idx;
                    })
                == 0) {
                state->config->songs.push_back(std::move(configs[level_idx]->songs[song_idx]));
                // Regenerate the ids of the song because the ones
                // generated from the file loader might collide here.
                state->config->songs[i].id = i;
                selected_indicies[i] = selected_idx;
                break; // generate next song
            }
        }
    }

    log_info("Successfully generated level");
    state->thread_state = GeneratorThreadResult::Success;
}

} // thenewworld
