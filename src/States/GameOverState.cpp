/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <memory>
#include <thenewworld/Camera.hpp>
#include <thenewworld/Configuration.hpp>
#include <thenewworld/GameContext.hpp>
#include <thenewworld/States/GameOverState.hpp>
#include <thenewworld/States/MenuState.hpp>
#include <thenewworld/translation.hpp>

namespace thenewworld {

GameOverState::GameOverState(time_duration& duration)
{
    m_time_line = T_GO_TOOK
        + std::to_string(duration.minutes()) + T_GO_MIN_AND
        + std::to_string(duration.seconds()) + T_GO_SEC;
}

GameOverState::~GameOverState()
{
}

void GameOverState::init(GameContext&)
{
}

void GameOverState::event(GameContext&, SDL_Event& e)
{
    if ((e.type == SDL_MOUSEBUTTONDOWN)
        && (m_animation_state != GameOverAnimationState::Finishing)) {
        m_animation_progress = 0;
        m_animation_state = GameOverAnimationState::Finishing;
    }
}

void GameOverState::update(GameContext& ctx, int dt)
{
    switch (m_animation_state) {
    case GameOverAnimationState::Idle:
        break;
    case GameOverAnimationState::Starting: {
        if ((m_animation_progress += dt) >= m_animation_duration) {
            m_animation_progress = 0;
            m_animation_state = GameOverAnimationState::Idle;
        }
    } break;
    case GameOverAnimationState::Finishing: {
        if ((m_animation_progress += dt) >= m_animation_duration) {
            ctx.set_state(std::make_unique<MenuState>());
            ctx.start_background_music();
        }
    } break;
    }
}

void GameOverState::render(Camera& cam)
{
    Color text_color;
    Color bg_color;

    switch (m_animation_state) {
    case GameOverAnimationState::Idle: {
        text_color = GAMEOVER_TEXT_COLOR;
        bg_color = GAMEOVER_BACKGROUND_COLOR;
    } break;
    case GameOverAnimationState::Starting: {
        text_color = GAME_BACKGROUND_COLOR.interpolate(GAMEOVER_TEXT_COLOR,
            m_animation_progress,
            m_animation_duration);
        bg_color = GAMEOVER_BACKGROUND_COLOR;
    } break;
    case GameOverAnimationState::Finishing: {
        text_color = GAMEOVER_TEXT_COLOR.interpolate(Color::black,
            m_animation_progress,
            m_animation_duration);
        bg_color = GAMEOVER_BACKGROUND_COLOR.interpolate(Color::black,
            m_animation_progress,
            m_animation_duration);
    } break;
    }

    auto vp = cam.get_viewport();
    cam.fill_screen(bg_color);

    const std::string header = T_GO_WON;
    auto text_rect = cam.text_boundaries(header);
    text_rect.scale_by(0.6 * vp.w / text_rect.w);
    text_rect.y = 50;
    text_rect.x = (vp.w - text_rect.w) * 0.5;
    cam.render_text(header, text_color, text_rect);

    auto time_rect = cam.text_boundaries(m_time_line);
    time_rect.scale_by(0.8 * vp.w / time_rect.w);
    time_rect.y = text_rect.y + text_rect.h + 30;
    time_rect.x = (vp.w - time_rect.w) * 0.5;
    cam.render_text(m_time_line, text_color, time_rect);

    const std::string tap = T_GO_BACK;
    auto tap_rect = cam.text_boundaries(tap);
    tap_rect.scale_by(0.5 * vp.w / tap_rect.w);
    tap_rect.y = time_rect.y + time_rect.h + 30;
    tap_rect.x = (vp.w - tap_rect.w) * 0.5;
    cam.render_text(tap, text_color, tap_rect);
}

} // thenewworld
