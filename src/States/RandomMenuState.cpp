/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <thenewworld/Configuration.hpp>
#include <thenewworld/GameContext.hpp>
#include <thenewworld/States/ErrorState.hpp>
#include <thenewworld/States/GeneratingRandomLevelState.hpp>
#include <thenewworld/States/MenuState.hpp>
#include <thenewworld/States/RandomMenuState.hpp>
#include <thenewworld/translation.hpp>

#include <array>
#include <utility>

namespace thenewworld {

RandomMenuState::RandomMenuState()
{
    const std::array<std::pair<EntryType, const std::string>, (size_t)(EntryType::N)>
        entry_descriptions {
            { { EntryType::FourByFour, "4x4" },
                { EntryType::FiveByFour, "5x4" },
                { EntryType::FourByThree, "4x3" },
                { EntryType::Back, T_RND_Back } }
        };

    for (auto& description : entry_descriptions) {
        auto entry = std::make_unique<RandomMenuEntry>(description.second, description.first);
        m_menu.append_entry(std::move(entry));
    }

    m_menu.set_position_policy(PositionPolicy::Centered);
}

void RandomMenuState::update(GameContext& ctx, int dt)
{
    switch (m_animation_state) {
    case AnimationState::Idle:
        break;
    case AnimationState::Starting: {
        if ((m_animation_progress += dt) >= m_animation_duration) {
            m_animation_state = AnimationState::Idle;
        }
    } break;
    case AnimationState::GoingBack: {
        if ((m_animation_progress += dt) >= m_animation_duration) {
            auto new_state = std::make_unique<MenuState>();
            new_state->set_come_from_random_menu();
            ctx.set_state(std::move(new_state));
        }
    } break;
    case AnimationState::StartingGame: {
        if ((m_animation_progress += dt) >= m_animation_duration) {
            auto new_state = std::make_unique<GeneratingRandomLevelState>(m_width, m_height);
            ctx.set_state(std::move(new_state));
        }
    } break;
    }
}

void RandomMenuState::render(Camera& cam)
{
    auto text_color = MENU_TEXT_COLOR;
    const auto vp = cam.get_viewport();

    if (m_animation_state == AnimationState::StartingGame) {
        text_color = MENU_TEXT_COLOR.interpolate(Color::black, m_animation_progress, m_animation_duration);
        cam.fill_screen(
            MENU_BACKGROUND_COLOR.interpolate(
                Color::black,
                m_animation_progress,
                m_animation_duration));
    } else {
        // Clear the screen
        cam.fill_screen(MENU_BACKGROUND_COLOR);
    }

    auto progress = (double)(m_animation_progress) / (double)(m_animation_duration);
    if (m_animation_state == AnimationState::Starting) {
        vec2 translation { vp.w * pow(1.0 - progress, 4), 0.0 }; // a bit of easing
        cam.push_translation(translation);
    } else if (m_animation_state == AnimationState::GoingBack) {
        vec2 translation { vp.w * pow(progress, 4), 0.0 }; // a bit of easing
        cam.push_translation(translation);
    }

    // Let's render some kind of header first.
    // Steal the design from the main menu
    const std::string random_header = T_RND_BoardSize;
    auto header_rect = cam.text_boundaries(random_header);

    header_rect.scale_by(vp.w / header_rect.w * 0.8);
    header_rect.x = (vp.w - header_rect.w) * 0.5;
    header_rect.y = 50;
    cam.render_text(random_header, text_color, header_rect);

    double y0 = header_rect.y + header_rect.h + 20.0; // some padding

    // now put in the menu at the correct position
    m_menu.set_text_color(text_color);
    m_menu.set_y_position(y0);
    m_menu.set_entry_height(vp.h * 0.08);
    m_menu.render(cam);

    if (m_animation_state == AnimationState::Starting
        || m_animation_state == AnimationState::GoingBack) {
        cam.pop_translation();
    }
}

void RandomMenuState::select_item(GameContext& ctx)
{
    auto type = m_menu.current_selection_type();
    switch (type) {
    case EntryType::Back: {
        m_animation_progress = 0;
        m_animation_state = AnimationState::GoingBack;
    } break;
    default: {
        switch (type) {
        case EntryType::FourByFour:
            m_width = 4;
            m_height = 4;
            break;
        case EntryType::FiveByFour:
            m_width = 5;
            m_height = 4;
            break;
        case EntryType::FourByThree:
            m_width = 4;
            m_height = 3;
            break;
        case EntryType::N:
        case EntryType::Back: {
            ctx.set_state(std::make_unique<ErrorState>("Internal game error: "
                                                       "Tried to determine size of invalid menu entry."));
        }
            return;
        }

        m_animation_state = AnimationState::StartingGame;
        m_animation_progress = 0;
        m_animation_duration = 800;

        return;

    } break;
    }
}

void RandomMenuState::event(GameContext& ctx, SDL_Event& e)
{
    if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_RETURN) {
        select_item(ctx);
    } else if (e.type == SDL_MOUSEMOTION) {
        vec2 mouse_position { (double)(e.motion.x), (double)(e.motion.y) };
        if (m_menu.rect().contains(mouse_position)) {
            m_menu.on_mouse_hover_entries(mouse_position);
        }
    } else if (e.type == SDL_MOUSEBUTTONDOWN) {
        vec2 mouse_position { (double)(e.motion.x), (double)(e.motion.y) };
        if (m_menu.rect().contains(mouse_position)) {
            m_menu.on_mouse_hover_entries(mouse_position);
            select_item(ctx);
        }
    } else {
        m_menu.event(e);
    }
}

} // thenewworld
