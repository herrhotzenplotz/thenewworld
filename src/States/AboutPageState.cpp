/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <thenewworld/States/AboutPageState.hpp>
#include <thenewworld/States/MenuState.hpp>
#include <thenewworld/translation.hpp>

namespace thenewworld {

AboutPageState::AboutPageState()
    : m_header(T_ABOUT_HEADER)
    , m_footer(T_ABOUT_FOOTER)
    , m_content(T_ABOUT_CONTENT)
{
}

AboutPageState::~AboutPageState()
{
}

void AboutPageState::init(GameContext&)
{
}

void AboutPageState::event(GameContext&, SDL_Event& e)
{
    if ((e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_q)
        || (e.type == SDL_MOUSEBUTTONDOWN)) {
        m_animation_progress = 0;
        m_animation_state = AboutPageAnimationState::Exiting;
    }
}

void AboutPageState::update(GameContext& ctx, int dt)
{
    if (m_animation_state != AboutPageAnimationState::Idle)
        if ((m_animation_progress += dt) >= m_animation_duration) {
            if (m_animation_state == AboutPageAnimationState::Starting) {
                m_animation_state = AboutPageAnimationState::Idle;
            } else if (m_animation_state == AboutPageAnimationState::Exiting) {
                auto new_state = std::make_unique<MenuState>();
                ctx.set_state(std::move(new_state));
            }
        }
}

void AboutPageState::render(Camera& cam)
{
    cam.fill_screen(Color::black);
    auto vp = cam.get_viewport();

    Color font_col;
    switch (m_animation_state) {
    case AboutPageAnimationState::Starting: {
        font_col = Color::black.interpolate(Color::white,
            m_animation_progress,
            m_animation_duration);
    } break;
    case AboutPageAnimationState::Idle: {
        font_col = Color::white;
    } break;
    case AboutPageAnimationState::Exiting: {
        font_col = Color::white.interpolate(Color::black,
            m_animation_progress,
            m_animation_duration);
    } break;
    }

    auto title_rect = cam.text_boundaries(m_header);
    title_rect.scale_by(vp.h / title_rect.h * 0.1);
    title_rect.x = (vp.w - title_rect.w) * 0.5;
    title_rect.y = 50;

    auto content_y0 = title_rect.y + title_rect.h + 50; // some padding

    auto footer_rect = cam.text_boundaries(m_footer);
    footer_rect.scale_by(vp.h / footer_rect.h * 0.05);
    footer_rect.x = (vp.w - footer_rect.w) * 0.5;
    footer_rect.y = vp.h - footer_rect.h - 50.0; // some padding

    for (size_t i = 0; i < m_content.size(); ++i) {
        auto content_rect = cam.text_boundaries(m_content[i]);
        content_rect.scale_by(footer_rect.h / content_rect.h);
        content_rect.y = content_y0 + footer_rect.h * (double)(i);
        content_rect.x = (vp.w - content_rect.w) * 0.5;
        cam.render_text(m_content[i], font_col, content_rect);
    }

    cam.render_text(m_header, font_col, title_rect);
    cam.render_text(m_footer, font_col, footer_rect);
}

} // thenewworld
