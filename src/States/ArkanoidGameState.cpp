/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <cmath>
#include <thenewworld/GameContext.hpp>
#include <thenewworld/Logging.hpp>
#include <thenewworld/States/ArkanoidGameState.hpp>

#include <placeholder_png.h>

namespace thenewworld {

ArkanoidGameState::ArkanoidGameState(Color col, int velocity,
    double angle, double gravity,
    double friction)
    : m_gravity(gravity)
    , m_friction(friction)
    , m_col_rect(col)
{
    m_vel_x = (double)(velocity)*cos(angle);
    m_vel_y = (double)(velocity)*sin(angle);
    log_info("Starting Arkanoid");
}

void ArkanoidGameState::init(GameContext& ctx)
{
    m_image = ctx.camera()
                  .create_image(___assets_placeholder_png,
                      ___assets_placeholder_png_len);
    m_fly.start_animation("Aaaaaaaahhh!", 2200, 300);
}

void ArkanoidGameState::event(GameContext& ctx, SDL_Event& e)
{
    if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_q)
        ctx.quit();
    if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_k)
        m_col_rect.r = std::min(m_col_rect.r + 0.05, 1.0);
    if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_j)
        m_col_rect.r = std::max(m_col_rect.r - 0.05, 0.0);
    if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_SPACE)
        m_vel_y -= 12.0;

    if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_d)
        ctx.camera().set_black_white_mode(true);
    if (e.type == SDL_KEYUP && e.key.keysym.sym == SDLK_d)
        ctx.camera().set_black_white_mode(false);
}

void ArkanoidGameState::update(GameContext& ctx, int dt)
{
    m_vel_y += m_gravity;
    m_vel_x -= m_vel_x * m_friction;
    m_vel_y -= m_vel_y * m_friction;
    m_rect.x += m_vel_x;
    m_rect.y += m_vel_y;

    if (pow(abs(m_vel_x), 2) + pow(abs(m_vel_y), 2) <= 2e-1) {
        log_info("Too low speed");
        ctx.set_state(std::make_unique<ArkanoidGameState>(Color::beige,
            6,
            0.15 * 3.14159,
            1,
            0.002));
        return;
    }

    auto vp = ctx.camera().get_viewport();

    if (m_rect.x + m_rect.w >= vp.w) {
        m_vel_x *= -1.0f;
        m_rect.x = vp.w - m_rect.w;
    }

    if (m_rect.x < 0) {
        m_vel_x *= -1.0f;
        m_rect.x = 0;
    }

    if (m_rect.y + m_rect.h >= vp.h) {
        m_vel_y *= -1.0f;
        m_rect.y = vp.h - m_rect.h;
    }

    if (m_rect.y < 0) {
        m_vel_y *= -1.0f;
        m_rect.y = 0;
    }

    m_fly.update(dt);
}

void ArkanoidGameState::render(Camera& cam)
{
    cam.fill_screen(Color::gray);
    cam.render_image(m_image, m_rect);
    m_fly.render(cam);
}

} // thenewworld
