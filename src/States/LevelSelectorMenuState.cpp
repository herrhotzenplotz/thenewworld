/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <thenewworld/Configuration.hpp>
#include <thenewworld/GameContext.hpp>
#include <thenewworld/States/ErrorState.hpp>
#include <thenewworld/States/GameRunningState.hpp>
#include <thenewworld/States/RandomMenuState.hpp>
#include <thenewworld/States/LevelSelectorMenuState.hpp>
#include <thenewworld/States/MenuState.hpp>
#include <thenewworld/translation.hpp>

#include <array>
#include <utility>

#include <boost/filesystem.hpp>

namespace thenewworld {

LevelSelectorState::LevelSelectorState()
{
}

LevelSelectorState::RandomGameMenuEntry::RandomGameMenuEntry()
    : MenuEntry(T_RANDOM_GAME, EntryType::Random)
{
}

LevelSelectorState::AbortMenuEntry::AbortMenuEntry()
    : MenuEntry(T_ABORT_SELECT, EntryType::Abort)
{
}

LevelSelectorState::FileMenuEntry::FileMenuEntry(const std::string title, const std::string set_path)
    : MenuEntry(title, EntryType::File)
    , m_set_path(set_path)
{
}

void LevelSelectorState::init(GameContext&)
{
    log_info("Searching for song sets in data/");

    for (auto& path : boost::filesystem::directory_iterator("data/")) {
        if (boost::filesystem::is_regular_file(path) && path.path().extension() == ".yaml") {
            std::string title = path.path().stem().string();
            const char* set_path = path.path().string().c_str();
            auto entry = std::make_unique<FileMenuEntry>(title, set_path);
            m_menu.append_entry(std::move(entry));
        }
    }

    if (m_menu.empty())
        log_info("No song sets found");

    auto random_entry = std::make_unique<RandomGameMenuEntry>();
    m_menu.append_entry(std::move(random_entry));

    auto abort_entry = std::make_unique<AbortMenuEntry>();
    m_menu.append_entry(std::move(abort_entry));

    m_menu.set_position_policy(PositionPolicy::Centered);
}

void LevelSelectorState::update(GameContext& ctx, int dt)
{
    switch (m_animation_state) {
    case AnimationState::Idle:
        break;
    case AnimationState::Starting: {
        if ((m_animation_progress += dt) >= m_animation_duration) {
            m_animation_state = AnimationState::Idle;
        }
    } break;
    case AnimationState::Aborting: {
        if ((m_animation_progress += dt) >= m_animation_duration) {
            auto new_state = std::make_unique<MenuState>();
            new_state->set_come_from_random_menu();
            ctx.set_state(std::move(new_state));
        }
    } break;
    case AnimationState::Done: {
        if ((m_animation_progress += dt) >= m_animation_duration) {
            if (m_menu.current_selection_type() == EntryType::Random) {
                auto new_state = std::make_unique<RandomMenuState>();
                ctx.set_state(std::move(new_state));
                return;
            } else { // File
                auto* current_entry = dynamic_cast<FileMenuEntry*>(m_menu.current_entry_ptr());
                auto new_state = std::make_unique<GameRunningState>(current_entry->set_path());
                ctx.set_state(std::move(new_state));
                ctx.stop_background_music();
                return;
            }
        }
    } break;
    }
}

void LevelSelectorState::render(Camera& cam)
{
    auto text_color = MENU_TEXT_COLOR;
    const auto vp = cam.get_viewport();
    const auto type = m_menu.current_selection_type();

    if (m_animation_state == AnimationState::Done && type == EntryType::File) {
        text_color = MENU_TEXT_COLOR.interpolate(Color::black, m_animation_progress, m_animation_duration);
        cam.fill_screen(
            MENU_BACKGROUND_COLOR.interpolate(
                Color::black,
                m_animation_progress,
                m_animation_duration));
    } else {
        // Clear the screen
        cam.fill_screen(MENU_BACKGROUND_COLOR);
    }

    auto progress = (double)(m_animation_progress) / (double)(m_animation_duration);
    if (m_animation_state == AnimationState::Starting) {
        vec2 translation { vp.w * pow(1.0 - progress, 4), 0.0 }; // a bit of easing
        cam.push_translation(translation);
    } else if (m_animation_state == AnimationState::Aborting) {
        vec2 translation { vp.w * pow(progress, 4), 0.0 }; // a bit of easing
        cam.push_translation(translation);
    } else if (m_animation_state == AnimationState::Done && type == EntryType::Random) {
        vec2 translation { -1.0 * vp.w * pow(progress, 4), 0.0 }; // a bit of easing
        cam.push_translation(translation);
    }

    const std::string random_header = T_SELECT_LEVEL;
    auto header_rect = cam.text_boundaries(random_header);

    header_rect.scale_by(vp.w / header_rect.w * 0.8);
    header_rect.x = (vp.w - header_rect.w) * 0.5;
    header_rect.y = 50;
    cam.render_text(random_header, text_color, header_rect);

    double y0 = header_rect.y + header_rect.h + 20.0; // some padding

    m_menu.set_text_color(text_color);
    m_menu.set_y_position(y0);
    m_menu.set_entry_height(vp.h * 0.08);
    m_menu.render(cam);

    if (m_animation_state == AnimationState::Starting
        || m_animation_state == AnimationState::Aborting
        || (m_animation_state == AnimationState::Done && type == EntryType::Random)) {
        cam.pop_translation();
    }
}

void LevelSelectorState::select_item(GameContext&)
{
    auto type = m_menu.current_selection_type();
    switch (type) {
    case EntryType::Abort: {
        m_animation_progress = 0;
        m_animation_state = AnimationState::Aborting;
    } break;
    case EntryType::Random:
    case EntryType::File: {
        m_animation_progress = 0;
        m_animation_state = AnimationState::Done;
    } break;
    }
}

void LevelSelectorState::event(GameContext& ctx, SDL_Event& e)
{
    if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_RETURN) {
        select_item(ctx);
    } else if (e.type == SDL_MOUSEMOTION) {
        vec2 mouse_position { (double)(e.motion.x), (double)(e.motion.y) };
        if (m_menu.rect().contains(mouse_position)) {
            m_menu.on_mouse_hover_entries(mouse_position);
        }
    } else if (e.type == SDL_MOUSEBUTTONDOWN) {
        vec2 mouse_position { (double)(e.motion.x), (double)(e.motion.y) };
        if (m_menu.rect().contains(mouse_position)) {
            m_menu.on_mouse_hover_entries(mouse_position);
            select_item(ctx);
        }
    } else {
        m_menu.event(e);
    }
}

} // thenewworld
