/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <thenewworld/Configuration.hpp>
#include <thenewworld/SongConfig.hpp>
#include <thenewworld/States/ErrorState.hpp>
#include <thenewworld/States/GameOverState.hpp>
#include <thenewworld/States/MultiplayerRunningState.hpp>

namespace thenewworld {

// TODO(#67): Multiplayer mode does not support randomly generated levels
MultiplayerRunningState::MultiplayerRunningState(std::string config, std::vector<Player>&& players)
    : m_players(players)
    , m_current_player_idx(0)
    , m_config_path(config)
    , m_animation_state(MultiplayerAnimationState::Running)
    , m_animation_duration(400)
    , m_animation_progress(0)
{
}

MultiplayerRunningState::~MultiplayerRunningState()
{
    if (m_songs)
        delete m_songs;

    if (m_board)
        delete m_board;
}

void MultiplayerRunningState::init(GameContext& ctx)
{
    m_songs = SongConfig::parse_from_file(ctx, m_config_path);
    if (!m_songs) {
        ctx.set_state(std::make_unique<ErrorState>("Unable to load song configuration"));
        return;
    }

    m_songs->load_songs(ctx.audio_player());
    size_t total_songs = m_songs->sample_count();

    if (total_songs == 0) {
        ctx.set_state(std::make_unique<ErrorState>("Configuration does not contain any songs"));
        return;
    }

    size_t width = (size_t)std::floor(std::sqrt((double)total_songs));
    size_t height = total_songs / width;

    // This occurs when total_songs cannot form a rectangle. (i.e. I
    // cannot calculate a rectangle that has an area of total_songs)
    if ((width * height) != total_songs) {
        ctx.set_state(std::make_unique<ErrorState>("Configuration produced an invalid board."));
        log_warn("The number of samples (i.e. songs * 2) must form a rectangle.");
        log_warn("E.g. you have 6 songs, which is 12 samples that can form a rectangle of 3x4.");
        return;
    }

    m_board = new Slideboard(ctx, height, width, m_songs);
    m_board->recalculate_slides(ctx);
    m_board->load_images(ctx);
    m_board->set_hide_on_wrong_match(true);
}

void MultiplayerRunningState::update(GameContext& ctx, int dt)
{
    m_board->update(ctx, dt);
    m_fly_text.update(dt);
    if (m_animation_state == MultiplayerAnimationState::Finishing) {
        if ((m_animation_progress += dt) >= m_animation_duration) {
            auto play_time = m_board->play_time();
            // TODO(#71): The game over screen should display the score and the winner of a multiplayer game
            ctx.set_state(std::make_unique<GameOverState>(play_time));
        }
    }
}

void MultiplayerRunningState::event(GameContext& ctx, SDL_Event& e)
{
    if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_q) {
        ctx.quit();
        return;
    } else if (e.type == SDL_MOUSEBUTTONUP
        && m_board->rect().contains(vec2 { (double)e.button.x, (double)e.button.y })) {
        m_board->event(ctx, e);
        if (m_board->check_for_win()) {
            m_animation_state = MultiplayerAnimationState::Finishing;
            m_animation_progress = 0;
        } else if (m_board->was_incorrect_match()) {
            // TODO(#68): The music should stop on an incorrect match in the multiplayer mode
            m_fly_text.start_animation(next_player().name + ", you're next!", 2000, 400);
        }
    }
}

// TODO(#70): MultiplayerRunningState::render does not indicate the current player and points
void MultiplayerRunningState::render(Camera& cam)
{
    cam.fill_screen(GAME_BACKGROUND_COLOR);
    m_board->render(cam);
    m_fly_text.render(cam);
}

Player& MultiplayerRunningState::next_player()
{
    m_current_player_idx = (m_current_player_idx + 1) % m_players.size();
    return player();
}

Player& MultiplayerRunningState::player()
{
    return m_players[m_current_player_idx];
}

} // thenewworld
