/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <thenewworld/GameContext.hpp>
#include <thenewworld/States/ErrorState.hpp>
#include <thenewworld/States/MenuState.hpp>
#include <thenewworld/translation.hpp>
#include <unistd.h>

namespace thenewworld {

ErrorState::ErrorState(std::string message)
    : m_message(message)
{
    log_error("ERROR STATE REACHED");
    log_error(message);
}

void ErrorState::init(GameContext& ctx)
{
    auto vp = ctx.camera().get_viewport();
    m_bounce_rect.x = vp.w * 0.9;
    m_bounce_rect.y = vp.h - m_bounce_rect.h;
}

void ErrorState::update(GameContext& ctx, int dt)
{
    auto vp = ctx.camera().get_viewport();

    if ((m_counter += dt) >= m_reset_point) {
        m_velocity += m_velocity_kick;
        m_counter = 0;
    }

    m_velocity += m_gravity * (double)dt / 10.0;
    m_bounce_rect.y += m_velocity * m_drag * (double)dt / 10.0;

    if (m_bounce_rect.y + m_bounce_rect.h >= vp.h) {
        m_velocity *= -1.0;
        m_bounce_rect.y = (vp.h - m_bounce_rect.h);
    }

    if (m_bounce_rect.y < 0) {
        m_velocity *= -1.0;
        m_bounce_rect.y = 0;
    }
}

void ErrorState::event(GameContext& ctx, SDL_Event& e)
{
    if ((e.type == SDL_KEYDOWN
            && e.key.keysym.sym == SDLK_RETURN)
        || (e.type == SDL_MOUSEBUTTONDOWN)) {
        ctx.set_state(std::make_unique<MenuState>());
        return;
    }

    // Have a chance if we really fucked up.
    if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_q) {
        exit(EXIT_FAILURE);
    }
}

void ErrorState::print_line(Camera& cam, std::string text)
{
    auto vp = cam.get_viewport();
    auto text_rect = cam.text_boundaries(text);

    if (std::abs(m_text_height) < 1e1) {
        text_rect.scale_by(vp.w / text_rect.w * 0.4);
        m_text_height = text_rect.h;
    } else {
        text_rect.scale_by(m_text_height / text_rect.h);
    }

    text_rect.x = m_x0;
    text_rect.y = m_y0;
    cam.render_text(text, Color::white, text_rect);

    m_y0 += text_rect.h + m_padding;
}

void ErrorState::render(Camera& cam)
{
    m_y0 = m_y_margin;
    cam.fill_screen(Color::black);
    auto vp = cam.get_viewport();
    const std::string header = T_ERROR_HEADER;
    auto header_rect = cam.text_boundaries(header);
    header_rect.scale_by(vp.w / header_rect.w * 0.3);
    header_rect.x = m_x0;
    header_rect.y = m_y0;
    cam.render_text(header, Color::white, header_rect);
    m_y0 += header_rect.h + m_padding;

    print_line(cam, T_ERROR_MESSAGE);
    print_line(cam, T_ERROR_BACK);

    m_y0 += 50.0;

    print_line(cam, T_ERROR_WAS);
    print_line(cam, m_message);
    print_line(cam, T_ERROR_REFER_LOG);
    cam.draw_filled_rect(m_bounce_rect, Color::gray);
}

} // thenewworld
