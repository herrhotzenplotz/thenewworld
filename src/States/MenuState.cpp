/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <boost/filesystem.hpp>
#include <thenewworld/Camera.hpp>
#include <thenewworld/Configuration.hpp>
#include <thenewworld/GameContext.hpp>
#include <thenewworld/States/AboutPageState.hpp>
#include <thenewworld/States/ArkanoidGameState.hpp>
#include <thenewworld/States/GameRunningState.hpp>
#include <thenewworld/States/MenuState.hpp>
#include <thenewworld/States/MultiplayerRunningState.hpp>
#include <thenewworld/States/LevelSelectorMenuState.hpp>
#include <thenewworld/States/RandomMenuState.hpp>
#include <thenewworld/Logging.hpp>
#include <thenewworld/translation.hpp>

namespace thenewworld {

MenuState::MenuState()
{
}

MenuState::~MenuState()
{
}

MenuState::QuitMenuEntry::QuitMenuEntry()
    : MenuEntry(T_QUIT_GAME, MenuStateEntryType::Quit)
{
}

MenuState::AboutPageMenuEntry::AboutPageMenuEntry()
    : MenuEntry(T_ABOUT_PAGE, MenuStateEntryType::About)
{
}

MenuState::MultiplayerMenuEntry::MultiplayerMenuEntry()
    : MenuEntry(T_MULTIPLAYER_GAME, MenuStateEntryType::Multiplayer)
{
}

MenuState::SingleplayerMenuEntry::SingleplayerMenuEntry()
    : MenuEntry(T_SINGLEPLAYER_GAME, MenuStateEntryType::Singleplayer)
{
}

void MenuState::init(GameContext&)
{
    auto single_entry = std::make_unique<SingleplayerMenuEntry>();
    m_menu.append_entry(std::move(single_entry));

    auto multiplayer_entry = std::make_unique<MultiplayerMenuEntry>();
    m_menu.append_entry(std::move(multiplayer_entry));

    auto about_entry = std::make_unique<AboutPageMenuEntry>();
    m_menu.append_entry(std::move(about_entry));

    auto quit_entry = std::make_unique<QuitMenuEntry>();
    m_menu.append_entry(std::move(quit_entry));

    m_menu.set_position_policy(PositionPolicy::Centered);
}

bool MenuState::quit_request()
{
    if (m_menu.current_selection_type() == MenuStateEntryType::Quit) {
        m_animation_progress = 0;
        m_anim_state = MenuAnimationState::QuittingGame;
        return true;
    }

    return false;
}

bool MenuState::about_page_request()
{
    if (m_menu.current_selection_type() == MenuStateEntryType::About) {
        m_animation_progress = 0;
        m_anim_state = MenuAnimationState::StartingAboutPage;
        return true;
    }
    return false;
}

bool MenuState::single_game_request()
{
    if (m_menu.current_selection_type() == MenuStateEntryType::Singleplayer) {
        m_animation_progress = 0;
        m_anim_state = MenuAnimationState::StartingSubMenu;
        m_submenu_request = SubmenuRequestType::Singleplayer;
        return true;
    }
    return false;
}

bool MenuState::multi_game_request()
{
    if (m_menu.current_selection_type() == MenuStateEntryType::Multiplayer) {
        m_animation_progress = 0;
        m_anim_state = MenuAnimationState::StartingSubMenu;
        m_submenu_request = SubmenuRequestType::Multiplayer;
        return true;
    }
    return false;
}

void MenuState::event(GameContext& ctx, SDL_Event& e)
{
    if (m_anim_state == MenuAnimationState::QuittingGame
        || m_anim_state == MenuAnimationState::StartingAboutPage) {
        return;
    }

    if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_RETURN) {
        if (quit_request()) {
            return;
        } else if (single_game_request()) {
            return;
        } else if (about_page_request()) {
            return;
        } else if (multi_game_request()) {
            return;
        }
    } else if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_q) {
        ctx.quit();
    } else if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_k) {
        ctx.set_state(std::make_unique<ArkanoidGameState>(Color::pastel_red, 6, 3.141 * 0.15, 1.0, 0.002));
        return;
    } else if (e.type == SDL_MOUSEMOTION) {
        auto mouse_pos = vec2 { (double)e.motion.x, (double)e.motion.y };
        if (m_menu.rect().contains(mouse_pos)) {
            m_menu.on_mouse_hover_entries(mouse_pos);
        }
    } else if (e.type == SDL_MOUSEBUTTONDOWN) {
        auto mouse_pos = vec2 { (double)e.button.x, (double)e.button.y };
        if (m_menu.rect().contains(mouse_pos)) {
            m_menu.on_mouse_hover_entries(mouse_pos);
            if (quit_request()) {
                return;
            } else if (single_game_request()) {
                return;
            } else if (about_page_request()) {
                return;
            } else if (multi_game_request()) {
                return;
            }
        }
    } else {
        m_menu.event(e);
    }
}

void MenuState::update(GameContext& ctx, int dt)
{
    switch (m_anim_state) {
    case MenuAnimationState::Idle:
        break;
    case MenuAnimationState::ComeFromSubMenu:
    case MenuAnimationState::StartingMenu: {
        if ((m_animation_progress += dt) >= m_animation_duration)
            m_anim_state = MenuAnimationState::Idle;
    } break;
    case MenuAnimationState::StartingAboutPage: {
        if ((m_animation_progress += dt) >= m_animation_duration) {
            auto new_state = std::make_unique<AboutPageState>();
            ctx.set_state(std::move(new_state));
            return;
        }
    } break;
    case MenuAnimationState::QuittingGame: {
        if ((m_animation_progress += dt) >= m_animation_duration)
            ctx.quit();
    } break;
    case MenuAnimationState::StartingSubMenu: {
        if ((m_animation_progress += dt) >= m_animation_duration) {
            if (m_submenu_request == SubmenuRequestType::Multiplayer) {
                // TODO(#72): The multiplayer menu is not implemented.
                // Instead it aborts which is not ideal.
                enforce_msg(false, "Not implemented");
                abort();
            } else if (m_submenu_request == SubmenuRequestType::Singleplayer) {
                auto new_state = std::make_unique<LevelSelectorState>();
                ctx.set_state(std::move(new_state));
                return;
            }
        }
    } break;
    }
}

double MenuState::render_title_and_get_entries_y0(Camera& cam,
    const Color& text_color)
{
    auto vp = cam.get_viewport();
    const std::string game_title = T_GAME_TITLE;
    auto title_rect = cam.text_boundaries(game_title);
    title_rect.scale_by(vp.w / title_rect.w * 0.8);
    title_rect.x = (vp.w - title_rect.w) * 0.5;
    title_rect.y = 50;
    cam.render_text(game_title, text_color, title_rect);
    return title_rect.y + title_rect.h + 20;
}

void MenuState::render_idle(Camera& cam)
{
    cam.fill_screen(MENU_BACKGROUND_COLOR);
    auto text_color = MENU_TEXT_COLOR;
    auto vp = cam.get_viewport();
    auto y0 = render_title_and_get_entries_y0(cam, text_color);
    m_menu.set_text_color(text_color);
    m_menu.set_y_position(y0);
    m_menu.set_entry_height(vp.h * 0.08);
    m_menu.render(cam);
}

void MenuState::render_start_menu(Camera& cam)
{
    cam.fill_screen(Color::black.interpolate(MENU_BACKGROUND_COLOR,
        m_animation_progress,
        m_animation_duration));
    auto text_color = Color::black.interpolate(MENU_TEXT_COLOR,
        m_animation_progress,
        m_animation_duration);
    auto vp = cam.get_viewport();
    auto y0 = render_title_and_get_entries_y0(cam, text_color);
    m_menu.set_text_color(text_color);
    m_menu.set_y_position(y0);
    m_menu.set_entry_height(vp.h * 0.08);
    m_menu.render(cam);
}

void MenuState::render_quit_game(Camera& cam)
{
    cam.fill_screen(MENU_BACKGROUND_COLOR.interpolate(Color::black,
        m_animation_progress,
        m_animation_duration));
    auto text_color = MENU_TEXT_COLOR.interpolate(Color::black,
        m_animation_progress,
        m_animation_duration);
    auto vp = cam.get_viewport();
    auto y0 = render_title_and_get_entries_y0(cam, text_color);
    m_menu.set_text_color(text_color);
    m_menu.set_y_position(y0);
    m_menu.set_entry_height(vp.h * 0.08);
    m_menu.render(cam);
}

void MenuState::render_start_game(Camera& cam)
{
    cam.fill_screen(MENU_BACKGROUND_COLOR.interpolate(GAME_BACKGROUND_COLOR,
        m_animation_progress,
        m_animation_duration));
    auto text_color = MENU_BACKGROUND_COLOR.interpolate(GAME_BACKGROUND_COLOR,
        m_animation_progress,
        m_animation_duration);
    auto vp = cam.get_viewport();
    auto y0 = render_title_and_get_entries_y0(cam, text_color);
    m_menu.set_text_color(text_color);
    m_menu.set_y_position(y0);
    m_menu.set_entry_height(vp.h * 0.08);
    m_menu.render(cam);
}

void MenuState::render_sub_menu(Camera& cam, AnimationDirection dir)
{
    cam.fill_screen(MENU_BACKGROUND_COLOR);
    auto text_color = MENU_TEXT_COLOR;
    auto vp = cam.get_viewport();
    auto progress = (double)(m_animation_progress) / (double)(m_animation_duration);
    if (dir == AnimationDirection::Backward) {
        progress = 1.0 - progress;
    }
    vec2 translation { -1.0 * vp.w * pow(progress, 4), 0.0 }; // a bit of easing
    cam.push_translation(translation);
    auto y0 = render_title_and_get_entries_y0(cam, text_color);
    m_menu.set_text_color(text_color);
    m_menu.set_y_position(y0);
    m_menu.set_entry_height(vp.h * 0.08);
    m_menu.render(cam);
    cam.pop_translation();
}

void MenuState::render(Camera& cam)
{
    switch (m_anim_state) {
    case MenuAnimationState::Idle:
        render_idle(cam);
        break;
    case MenuAnimationState::StartingMenu:
        render_start_menu(cam);
        break;
    case MenuAnimationState::StartingSubMenu:
        render_sub_menu(cam, AnimationDirection::Forward);
        break;
    case MenuAnimationState::ComeFromSubMenu:
        render_sub_menu(cam, AnimationDirection::Backward);
        break;
    case MenuAnimationState::StartingAboutPage:
    case MenuAnimationState::QuittingGame:
        render_quit_game(cam);
        break;
    }
}

} // thenewworld
