/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <cmath>
#include <thenewworld/Configuration.hpp>
#include <thenewworld/GameContext.hpp>
#include <thenewworld/States/ErrorState.hpp>
#include <thenewworld/States/GameOverState.hpp>
#include <thenewworld/States/GameRunningState.hpp>

namespace thenewworld {

GameRunningState::GameRunningState(std::string path)
    : m_config_path(path)
    , m_load_mode(GameLoadMode::FromFile)
{
}

GameRunningState::GameRunningState(SongConfig* config)
    : m_songs(config)
    , m_config_path()
    , m_load_mode(GameLoadMode::FromMemoryConfig)
{
}

GameRunningState::~GameRunningState()
{
    if (m_songs) {
        log_info("Freeing samples.");
        m_songs->free_samples();
        log_info("Deleting songs");
        delete m_songs;
    }

    if (m_board) {
        log_info("Deleting board");
        delete m_board;
    }
}

GameRunningState::GameRunningState(GameRunningState&& other)
    : m_board(std::move(other.m_board))
    , m_songs(std::move(other.m_songs))
{
}

void GameRunningState::init(GameContext& ctx)
{
    // Only load up config from a file if the the song config was
    // provided as a file. This might not be the case if the level was
    // randomly generated.
    if (m_load_mode == GameLoadMode::FromFile) {
        m_songs = SongConfig::parse_from_file(ctx, m_config_path);
        if (!m_songs) {
            return;
        }
    }

    m_songs->load_songs(ctx.audio_player());
    size_t total_songs = m_songs->sample_count();

    if (total_songs == 0) {
        ctx.set_state(std::make_unique<ErrorState>("Configuration does not contain any songs."));
        return;
    }

    size_t width = (size_t)std::floor(std::sqrt((double)total_songs));
    size_t height = total_songs / width;

    // This occurs when total_songs cannot form a rectangle. (i.e. I
    // cannot calculate a rectangle that has an area of total_songs)
    if ((width * height) != total_songs) {
        ctx.set_state(std::make_unique<ErrorState>("Configuration produced an invalid board."));
        log_warn("The number of samples (i.e. songs * 2) must form a rectangle.");
        log_warn("E.g. you have 6 songs, which is 12 samples that can form a rectangle of 3x4.");
        return;
    }

    m_board = new Slideboard(ctx, height, width, m_songs);
    m_board->recalculate_slides(ctx);
    m_board->load_images(ctx);
}

void GameRunningState::event(GameContext& ctx, SDL_Event& e)
{
    if (e.type == SDL_KEYDOWN && e.key.keysym.sym == SDLK_q) {
        ctx.quit();
        return;
    } else {
        m_board->event(ctx, e);
        if (m_board->check_for_win()) {
            m_animation_state = GameRunningAnimationState::Finishing;
            m_animation_progress = 0;
        }
    }
}

void GameRunningState::update(GameContext& ctx, int dt)
{
    m_board->update(ctx, dt);
    if (m_animation_state == GameRunningAnimationState::Finishing) {
        if ((m_animation_progress += dt) >= m_animation_duration) {
            // FIXME(#49): Determining the play time at the end of
            // animations is inaccurate
            auto play_time = m_board->play_time();
            ctx.set_state(std::make_unique<GameOverState>(play_time));
        }
    }
}

void GameRunningState::render(Camera& cam)
{
    if (m_animation_state == GameRunningAnimationState::Idle) {
        cam.fill_screen(GAME_BACKGROUND_COLOR);
        m_board->render(cam);
    } else if (m_animation_state == GameRunningAnimationState::Finishing) {
        auto bg_color = GAME_BACKGROUND_COLOR.interpolate(GAMEOVER_BACKGROUND_COLOR,
            m_animation_progress,
            m_animation_duration);
        cam.fill_screen(bg_color);
    }
}

} // thenewworld
