/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <thenewworld/Camera.hpp>
#include <thenewworld/Configuration.hpp>
#include <thenewworld/SDLAudio.hpp>
#include <thenewworld/Slide.hpp>

namespace thenewworld {

Slide::Slide(int id, const SoundSample* sample)
    : m_id(id)
    , m_title(std::to_string(id))
    , m_sample(sample)
{
}

void Slide::set_dimensions(Rect& rect)
{
    m_width = rect.w;
    m_height = rect.h;
}

void Slide::set_is_open(bool state)
{
    if (m_is_open == state)
        return;
    m_is_open = state;

    m_animation_state = state
        ? SlideAnimationState::Opening
        : SlideAnimationState::Failing;

    m_animation_progress = 0;
}

bool Slide::toggle_is_open()
{
    return m_is_open = !m_is_open;
}

void Slide::done()
{
    m_is_open = false;
    m_is_matched = true;
    m_animation_progress = 0;
    m_animation_state = SlideAnimationState::Finishing;
}

void Slide::fail()
{
    m_failed = true;
    m_is_open = false;
    m_animation_progress = 0;
    m_animation_state = SlideAnimationState::Failing;
}

void Slide::set_image(SDLImage* ptr)
{
    m_image = ptr;
}

void Slide::update(GameContext&, int dt)
{
    if (m_animation_state != SlideAnimationState::Idle) {
        if ((m_animation_progress += dt) >= m_animation_duration) {
            m_animation_state = SlideAnimationState::Idle;
            m_animation_progress = 0;
        }
    }
}

void Slide::play_sample(SDLAudioPlayer& player) const
{
    player.play_sample(m_sample);
}

void Slide::render(Camera& cam)
{
    if (m_is_matched && (m_animation_state == SlideAnimationState::Idle))
        return;

    auto r = Rect { m_width, m_height, 0, 0 };

    switch (m_animation_state) {
    case SlideAnimationState::Idle: {
        cam.draw_filled_rect(r, m_is_open ? SLIDE_OPENED_COLOR : SLIDE_CLOSED_COLOR);
        cam.render_image(m_image, r);
    } break;
    case SlideAnimationState::Failing: {
        auto col = SLIDE_OPENED_COLOR.interpolate(SLIDE_CLOSED_COLOR,
            m_animation_progress,
            m_animation_duration);
        cam.draw_filled_rect(r, col);
        cam.render_image(m_image, r);
    } break;
    case SlideAnimationState::Opening: {
        auto col = SLIDE_CLOSED_COLOR.interpolate(SLIDE_OPENED_COLOR,
            m_animation_progress,
            m_animation_duration);
        cam.draw_filled_rect(r, col);
        cam.render_image(m_image, r);
    } break;
    case SlideAnimationState::Finishing: {
        auto anim_rect = r.animate_scale(m_animation_progress,
            m_animation_duration);
        auto col = SLIDE_OPENED_COLOR.interpolate(GAME_BACKGROUND_COLOR,
            m_animation_progress,
            m_animation_duration);
        cam.draw_filled_rect(anim_rect, col);
        cam.render_image(m_image, anim_rect);
    } break;
    }
}

} // thenewworld
