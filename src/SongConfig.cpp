/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <boost/date_time/posix_time/posix_time.hpp>
#include <cassert>
#include <fstream>
#include <thenewworld/GameContext.hpp>
#include <thenewworld/Logging.hpp>
#include <thenewworld/SongConfig.hpp>
#include <thenewworld/States/ErrorState.hpp>
#include <thenewworld/translation.hpp>

namespace thenewworld {

double string_to_secs(std::string time)
{
    boost::posix_time::time_duration duration(boost::posix_time::duration_from_string(time));
    return ((double)duration.total_milliseconds()) / 1000.0;
}

SongConfig* SongConfig::parse_unsafe(std::string path)
{
    const YAML::Node doc = YAML::LoadFile(path);
    auto& config = *new SongConfig;
    config.name = path;

    int song_id = 0;
    for (auto& entry : doc) {
        Song song;
        song.id = song_id++;
        song.filepath = entry["file"].as<std::string>();
        song.title = entry["title"].as<std::string>();

        if (entry["movement"])
            song.movement = entry["movement"].as<std::string>();

        song.composer = entry["composer"].as<std::string>();
        int offset_id = 0;
        for (auto& sample_entry : entry["samples"]) {
            OffsetSample sample;
            auto start = string_to_secs(sample_entry["start"].as<std::string>());
            auto end = string_to_secs(sample_entry["end"].as<std::string>());
            sample.start = start;
            sample.len = end - start;
            sample.id = offset_id++;
            song.offset_samples[offset_id - 1] = std::move(sample);
        }

        config.songs.push_back(std::move(song));
    }
    return &config;
}

SongConfig* SongConfig::parse_from_file(GameContext& ctx, std::string path)
{
    try {
        return parse_unsafe(path);
    } catch (YAML::Exception& e) {
        std::string error_message("'" + path + "': " + e.what());
        log_warn(error_message);
        ctx.set_state(std::make_unique<ErrorState>(error_message));
        return nullptr;
    }
}

SongConfig* SongConfig::empty()
{
    return new SongConfig();
}

std::string Song::description() const
{
    if (movement.empty()) {
        return title + T_SONG_BY + composer;
    } else {
        return title + ": " + movement + T_SONG_BY + composer;
    }
}

void SongConfig::free_samples()
{
    for (auto& song : songs)
        song.free_buffers();
}

size_t SongConfig::sample_count() const
{
    size_t total = 0;
    for (auto& song : songs)
        total += song.samples.size();
    return total;
}

void Song::free_buffers()
{
    for (auto& sample : samples) {
        std::free(sample.m_audio_buf);
        sample.m_audio_len = 0;
    }
}

void SongConfig::load_songs(SDLAudioPlayer& player)
{
    for (auto& song : songs) {
        song.load_samples(player);
    }
}

/* Because holding Gigabytes worth of raw audio data in memory is
 * somewhat inefficient, we have precalculated the offsets of the raw
 * audio samples into the decoded buffer during the call of
 * Song::parse_from_file. We now use this data the following way: We
 * map the data into the memory and memcpy from the base to the mapped
 * sample data offset by the precalculated value sample.len bytes into
 * a freshly allocated buffer. This new buffer will be moved into the
 * samples and afterwards the raw wave data is freeed.
 */
void Song::load_samples(SDLAudioPlayer& player)
{
    auto ends_with = [](std::string str, std::string suffix) {
        if (suffix.size() > str.size())
            return false;

        for (int str_i = str.size() - 1, suff_i = suffix.size() - 1;
             suff_i == 0;
             suff_i--, str_i--) {
            if (str[str_i] != suffix[suff_i])
                return false;
        }

        return true;
    };

    // For reasons of efficiency, we handle loading mp3 differently than
    // wave data
    if (ends_with(filepath, ".mp3")) {
        auto mp3_samples = player.load_mp3_file(filepath, offset_samples);
        enforce(mp3_samples, "Unable to load mp3 samples");
        for (size_t i = 0; i < offset_samples.size(); ++i) {
            samples[i] = std::move(mp3_samples[i]);
        }
    } else if (ends_with(filepath, ".wav")) {
        auto wav_samples = player.load_wav_file(filepath, offset_samples);
        for (size_t i = 0; i < offset_samples.size(); ++i) {
            samples[i] = std::move(wav_samples[i]);
        }
    } else
        enforce_msg(false, "Unsupported audio format.");
}

} // thenewworld
