/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <SDL2/SDL.h>
#include <cassert>
#include <thenewworld/Logging.hpp>
#include <thenewworld/SDLWindow.hpp>

namespace thenewworld {

SDLWindow::SDLWindow(std::string title, bool force_opengl)
{
    log_info("Enumerating displays");

    SDL_DisplayMode current;
    for (int i = 0; i < SDL_GetNumVideoDisplays(); ++i) {
        enforce(SDL_GetDesktopDisplayMode(i, &current) == 0,
            "SDL_GetDesktopDisplayMode");
        log_info("Display " + std::to_string(i) + ": " + std::to_string(current.w)
            + "x" + std::to_string(current.h));
    }

    log_info("Creating SDL Window");
    m_window = SDL_CreateWindow(title.c_str(),
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        current.w, current.h,
        SDL_WINDOW_SHOWN | SDL_WINDOW_FULLSCREEN_DESKTOP);

    enforce(m_window != nullptr,
        "Could not create SDL Window");
    m_camera = std::make_unique<Camera>(m_window, force_opengl);
}

SDLWindow::~SDLWindow()
{
    log_info("Destroying SDL Window.");
    SDL_DestroyWindow(m_window);
}

void SDLWindow::present()
{
    camera().present();
}

double SDLWindow::calculate_display_scale()
{
    int w0 = 0;
    SDL_GetWindowSize(m_window, &w0, nullptr);
    assert(((void)"Window width cannot be less than or equal to zero", w0 > 0));
    int w1 = m_camera->render_width();
    assert(((void)"Render width cannot be less than zero", w1 >= 0));
    return (double)(w0) / (double)(w1);
}

SDL_Event SDLWindow::preprocess_sdl_event(SDL_Event e)
{
    switch (e.type) {
    case SDL_WINDOWEVENT: {

        switch (e.window.event) {
        case SDL_WINDOWEVENT_MOVED:
        case SDL_WINDOWEVENT_SIZE_CHANGED: {
            m_display_scale = calculate_display_scale();
            log_info("Window has been resized. New display scale: " + std::to_string(m_display_scale));
        } break;
        default:
            break;
        }

    } break;

    case SDL_MOUSEMOTION: {
        e.motion.x = (Sint32)((double)e.motion.x * m_display_scale);
        e.motion.y = (Sint32)((double)e.motion.y * m_display_scale);
    } break;

    case SDL_MOUSEBUTTONUP:
    case SDL_MOUSEBUTTONDOWN: {
        e.button.x = (Sint32)((double)e.button.x * m_display_scale);
        e.button.y = (Sint32)((double)e.button.y * m_display_scale);
    } break;

    default:
        break;
    }

    return e;
}

} // thenewworld
