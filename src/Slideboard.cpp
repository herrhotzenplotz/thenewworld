/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <algorithm>
#include <boost/filesystem.hpp>
#include <cmath>
#include <iterator>
#include <random>
#include <thenewworld/GameContext.hpp>
#include <thenewworld/Logging.hpp>
#include <thenewworld/Slideboard.hpp>
#include <thenewworld/States/ErrorState.hpp>

// Embedded assets
#include <accidentials_png.h>
#include <clef_bass_png.h>
#include <clef_treble_png.h>
#include <notes_png.h>

namespace thenewworld {

Slideboard::Slideboard(GameContext& ctx, size_t cols,
    size_t rows, SongConfig* config)
    : m_cols(cols)
    , m_rows(rows)
    , m_info_renderer(config)
{
    if (cols == 0 || rows == 0) {
        ctx.set_state(std::make_unique<ErrorState>("Tried to construct a Slideboard with a zero side length."));
        return;
    }

    for (auto& song : config->songs) {
        int id = song.id;
        for (auto& sample : song.samples) {
            auto slide = Slide { id, &sample };
            m_slides.push_back(std::move(slide));
        }
    }

    shuffle(m_slides.begin(), m_slides.end(), ctx.prng());
    m_start_time = ptime(second_clock::local_time());
}

void Slideboard::recalculate_slides(GameContext& ctx)
{
    auto vp = ctx.camera().get_viewport();
    auto single_slide_width = ((vp.w - 2 * m_padding) / (double)m_cols) - m_gap_size;
    auto single_slide_height = ((vp.h - 2 * m_padding) / (double)m_rows) - m_gap_size;

    auto shortest_side = std::min(single_slide_width, single_slide_height);

    m_rect.w = (shortest_side + m_gap_size) * (double)m_cols;
    m_rect.h = (shortest_side + m_gap_size) * (double)m_rows;
    m_rect.x = (vp.w - m_rect.w) / 2.0;
    m_rect.y = (vp.h - m_rect.h) / 2.0;

    m_slide_dims = Rect { shortest_side, shortest_side, 0, 0 };
    for (auto& slide : m_slides) {
        slide.set_dimensions(m_slide_dims);
    }
}

void Slideboard::load_images(GameContext& ctx)
{
    std::pair<void*, size_t> images[] = {
        { ___assets_notes_png, ___assets_notes_png_len },
        { ___assets_accidentials_png, ___assets_accidentials_png_len },
        { ___assets_clef_bass_png, ___assets_clef_bass_png_len },
        { ___assets_clef_treble_png, ___assets_clef_treble_png_len }
    };

    for (auto& p : images) {
        auto* img = ctx.camera().create_image(p.first, p.second);
        m_slide_images.push_back(img);
    }

    if (m_slide_images.empty()) {
        ctx.set_state(std::make_unique<ErrorState>("No images found."));
        return;
    }

    size_t max_i = m_slide_images.size();
    for (auto& slide : m_slides) {
        slide.set_image((m_slide_images[(ctx.prng()()) % max_i]));
    }
}

Slide& Slideboard::slide_at(size_t x, size_t y)
{
    enforce_lambda([]() { return "Out of range"; },
        x <= m_cols && y <= m_rows,
        "Slideboard::slide_at");
    return m_slides[y * m_cols + x];
}

void Slideboard::set_slide_at(size_t x, size_t y, Slide&& slide)
{
    enforce_lambda([]() { return "Out of range"; },
        x <= m_cols && y <= m_rows,
        "Slideboard::set_slide_at");
    m_slides[y * m_cols + x] = std::move(slide);
}

void Slideboard::click_on_slides(SDLAudioPlayer& audio_player,
    vec2& coord)
{
    if (!m_can_click)
        return;
    auto top = m_rect.top();
    auto in_board = coord - top;
    auto cell_x = (size_t)floor(in_board.x / (m_slide_dims.w + m_gap_size));
    auto cell_y = (size_t)floor(in_board.y / (m_slide_dims.h + m_gap_size));

    auto& slide = slide_at(cell_x, cell_y);
    if (slide.is_done())
        return;

    if (!m_selected_slide_1) {
        m_selected_slide_1 = boost::make_optional(&slide);
        slide.set_is_open(true);
        slide.play_sample(audio_player);
    } else if (!m_selected_slide_2) {
        // TODO(#53): Clicking on a slide multiple times will play the sample over and over
        slide.play_sample(audio_player);
        if (slide.is_open() && !slide.is_done())
            return;
        m_selected_slide_2 = boost::make_optional(&slide);
        slide.set_is_open(true);
        m_can_click = false;
        check_if_matched();
    }
}

void Slideboard::clear_selection()
{
    m_selected_slide_1 = boost::none;
    m_selected_slide_2 = boost::none;
    m_can_click = true;
}

void Slideboard::match_correct()
{
    m_selected_slide_1.value()->done();
    m_selected_slide_2.value()->done();
    m_info_renderer.show(m_selected_slide_1.value()->id());
    clear_selection();
}

void Slideboard::match_failed()
{
    if (m_hide_on_wrong_match) {
        m_selected_slide_1.value()->fail();
        m_selected_slide_2.value()->fail();
        clear_selection();
        m_was_incorrect_match = true;
    } else {
        m_selected_slide_1.value()->fail();
        m_selected_slide_1 = boost::make_optional(m_selected_slide_2.value());
        m_selected_slide_2 = boost::none;
        m_can_click = true;
    }
}

bool Slideboard::check_if_matched()
{
    if (m_selected_slide_1.value()->id() == m_selected_slide_2.value()->id()) {
        match_correct();
        return true;
    } else {
        match_failed();
        return false;
    }
}

time_duration Slideboard::play_time()
{
    ptime end(second_clock::local_time());
    auto diff = end - m_start_time;
    return diff;
}

bool Slideboard::check_for_win()
{
    return all_of(m_slides.begin(), m_slides.end(),
        [](auto& slide) { return slide.is_done(); });
}

bool Slideboard::was_incorrect_match() const
{
    return m_was_incorrect_match;
}

////////////////////////////// LOGIC //////////////////////////////
void Slideboard::event(GameContext& ctx, SDL_Event& event)
{
    if (event.type == SDL_WINDOWEVENT && event.window.event == SDL_WINDOWEVENT_SIZE_CHANGED) {
        recalculate_slides(ctx);
    } else {
        switch (event.type) {
        case SDL_MOUSEBUTTONUP: {
            auto coord = vec2 { (double)event.button.x, (double)event.button.y };
            if (m_rect.contains(coord)) {
                click_on_slides(ctx.audio_player(), coord);
            }

        } break;
        default:
            break;
        }
    }
}

void Slideboard::update(GameContext& ctx, int dt)
{
    m_was_incorrect_match = false;

    m_info_renderer.update(dt);

    for (auto& slide : m_slides)
        slide.update(ctx, dt);
}

void Slideboard::render(Camera& cam)
{
    m_info_renderer.render(cam);
    for (size_t y = 0; y < m_rows; ++y) {
        for (size_t x = 0; x < m_cols; ++x) {
            cam.push_translation({ m_rect.x + (double)x * (m_slide_dims.w + m_gap_size),
                m_rect.y + (double)y * (m_slide_dims.h + m_gap_size) });
            m_slides[y * m_cols + x].render(cam);
            cam.pop_translation();
        }
    }
}

} // thenewworld
