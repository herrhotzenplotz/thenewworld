/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <cmath>
#include <thenewworld/Color.hpp>

namespace thenewworld {
Color::Color(double r, double g, double b, double a)
    : r(r)
    , g(g)
    , b(b)
    , a(a)
{
}

Color Color::desaturate() const
{
    double k = (r + g + b) / 3.0f;
    return { k, k, k, a };
}

Color Color::interpolate(const Color& other, const int t, const int tend) const
{
    double pt = pow((double)t / (double)tend, 0.25);

    return { r * (1.0 - pt) + other.r * pt,
        g * (1.0 - pt) + other.g * pt,
        b * (1.0 - pt) + other.b * pt,
        a * (1.0 - pt) + other.a * pt };
}

const Color Color::black = { 0.0, 0.0, 0.0, 1.0 };
const Color Color::white = { 1.0, 1.0, 1.0, 1.0 };
const Color Color::beige = from_hex(0xEAD59E);
const Color Color::gray = from_hex(0x444F59);
const Color Color::olive = from_hex(0x7D7A45);
const Color Color::wine = from_hex(0x5C2722);
const Color Color::pastel_red = from_hex(0xC75E49);
const Color Color::green = from_hex(0x5E7F6E);

} // thenewworld
