/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

// ----- WARNING! ------------------------------------------------
// This line needs to be included before the SoundSample.hpp include
// in order to build the actual C implementation of minimp3.  If we
// would it do the other way around, we get undefined references
// because the include guard in minimp3 would not include the
// implementation of the symbols.
#define MINIMP3_IMPLEMENTATION
#include <minimp3/minimp3_ex.h>
// ---------------------------------------------------------------

#include <string>
#include <thenewworld/Logging.hpp>
#include <thenewworld/SDLAudio.hpp>
#include <thenewworld/SoundSample.hpp>

#include <boost/iostreams/device/mapped_file.hpp>

#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>

namespace thenewworld {

SDLAudioPlayer::SDLAudioPlayer()
{
    SDL_AudioSpec wanted;
    wanted.freq = 44100;
    wanted.format = AUDIO_S16;
    wanted.samples = 4096;;
    wanted.channels = 2;
    wanted.callback = nullptr;
    wanted.samples = 4096;
    log_info("Creating SDL audio device");
    enforce((m_dev = SDL_OpenAudioDevice(nullptr, false, &wanted, &m_audiospec, 1)),
        "SDL_OpenAudio");
}

SDLAudioPlayer::~SDLAudioPlayer()
{
    log_info("Closing SDL audio device");
    SDL_CloseAudio();
}

SoundSample* SDLAudioPlayer::load_sample_from_wav_file(const std::string& path)
{
    SDL_AudioSpec* got;
    Uint8* audio_data;
    Uint32 audio_len;
    enforce(got = SDL_LoadWAV(path.c_str(),
                &m_audiospec,
                &audio_data,
                &audio_len),
        "SDL_LoadWAV");
    enforce(SDL_BuildAudioCVT(&m_cvt, got->format, got->channels, got->freq,
                m_audiospec.format, m_audiospec.channels,
                m_audiospec.freq)
            >= 0,
        "SDL_BuildAudioCVT");
    m_cvt.buf = audio_data;
    m_cvt.len = (int)audio_len;
    enforce(SDL_ConvertAudio(&m_cvt) == 0,
        "SDL_ConvertAudio");
    auto* sample = new SoundSample;
    sample->m_audio_len = (size_t)m_cvt.len_cvt;
    sample->m_audio_buf = m_cvt.buf;
    return sample;
}

SoundSample* SDLAudioPlayer::load_from_mp3_buffer(void* buffer, size_t buffer_size)
{
    auto dec = new mp3dec_ex_t;
    enforce(!mp3dec_ex_open_buf(dec, (const uint8_t*)buffer,
                buffer_size, MP3D_SEEK_TO_SAMPLE),
        "Unable to open input mp3 data stream");

    enforce(SDL_BuildAudioCVT(&m_cvt, AUDIO_S16, dec->info.channels, dec->info.hz,
                m_audiospec.format, m_audiospec.channels,
                m_audiospec.freq)
            >= 0,
        "SDL_BuildAudioCVT");

    size_t expected_samples = dec->samples;

    auto* sample = new SoundSample;

    auto* temp_buf = new Uint8[expected_samples * sizeof(mp3d_sample_t) * m_cvt.len_mult];
    auto read_samples = mp3dec_ex_read(dec, (mp3d_sample_t*)temp_buf,
        expected_samples);
    enforce((read_samples == expected_samples) || dec->last_error,
        "Unable to read required bytes from file");

    m_cvt.buf = (Uint8*)temp_buf;
    m_cvt.len = (int)(expected_samples * sizeof(mp3d_sample_t));

    enforce(SDL_ConvertAudio(&m_cvt) == 0,
        "SDL_ConvertAudio");

    sample->m_audio_len = (size_t)m_cvt.len_cvt;
    sample->m_audio_buf = m_cvt.buf;

    mp3dec_ex_close(dec);

    delete dec;

    return sample;
}

void SDLAudioPlayer::stop_playback()
{
    SDL_PauseAudioDevice(m_dev, 1);
    SDL_ClearQueuedAudio(m_dev);
}

void SDLAudioPlayer::play_sample(const SoundSample* sample)
{
    SDL_ClearQueuedAudio(m_dev);
    enforce(SDL_QueueAudio(m_dev, sample->m_audio_buf, sample->m_audio_len) == 0,
        "SDL_QueueAudio");
    SDL_PauseAudioDevice(m_dev, 0);
}

SoundSample* SDLAudioPlayer::load_wav_file(const std::string& path, std::array<OffsetSample, 2>& offsets)
{
    SDL_AudioSpec* got;
    Uint8* audio_data;
    Uint32 audio_len;
    enforce(got = SDL_LoadWAV(path.c_str(),
                &m_audiospec,
                &audio_data,
                &audio_len),
        "SDL_LoadWAV");
    enforce(SDL_BuildAudioCVT(&m_cvt, got->format, got->channels, got->freq,
                m_audiospec.format, m_audiospec.channels,
                m_audiospec.freq)
            >= 0,
        "SDL_BuildAudioCVT");
    m_cvt.buf = audio_data;
    m_cvt.len = (int)audio_len;
    enforce(SDL_ConvertAudio(&m_cvt) == 0,
        "SDL_ConvertAudio");
    auto* samples = new SoundSample[offsets.size()];

    for (size_t i = 0; i < offsets.size(); ++i) {
        auto& offset = offsets[i];
        size_t expected_samples = (size_t)((double)(got->freq * got->channels) * offset.len);
        size_t offset_samples = (size_t)((double)(got->freq * got->channels) * offset.start);

        samples[i].m_audio_len = expected_samples * sizeof(uint32_t) * m_cvt.len_mult;
        samples[i].m_audio_buf = new Uint8[expected_samples * sizeof(mp3d_sample_t) * m_cvt.len_mult];
        std::memcpy(samples[i].m_audio_buf, m_cvt.buf + offset_samples * sizeof(uint32_t),
            expected_samples * sizeof(uint32_t) * m_cvt.len_mult);
    }

    SDL_FreeWAV(audio_data);

    return samples;
}

SoundSample* SDLAudioPlayer::load_mp3_file(const std::string& path, std::array<OffsetSample, 2>& offsets)
{
    auto dec = new mp3dec_ex_t;
    enforce(!mp3dec_ex_open(dec, path.c_str(), MP3D_SEEK_TO_SAMPLE),
        "Unable to open input mp3 data stream");

    // FIXME(#48): Converting sample format is not architecture independent
    // This depends on short being a signed 16-bit value (which is the
    // case on x86_64)
    enforce(SDL_BuildAudioCVT(&m_cvt, AUDIO_S16, dec->info.channels, dec->info.hz,
                m_audiospec.format, m_audiospec.channels,
                m_audiospec.freq)
            >= 0,
        "SDL_BuildAudioCVT");

    auto* samples = new SoundSample[offsets.size()];

    for (size_t i = 0; i < offsets.size(); ++i) {
        size_t expected_samples = (size_t)((double)(dec->info.hz * dec->info.channels) * offsets[i].len);
        size_t offset_samples = (size_t)((double)(dec->info.hz * dec->info.channels) * offsets[i].start);

        // We need to handle this case seperate as we want to
        // catch this error at different places differently.  In
        // the game we want to fail the whole application but in
        // the level editor we don't want to crash for the better
        // ux. So we need a generic way to indicate that.

        if (offsets[i].start < 0 || offsets[i].len < 0) {
            delete dec;
            delete[] samples;
            return nullptr;
        }

        if (mp3dec_ex_seek(dec, offset_samples) != 0) {
            delete dec;
            delete[] samples;
            return nullptr;
        }

        /* Nasty bug warning: Excerpt from SDL2 docs: len_mult is the
         * length multiplier for determining the size of the converted
         * data. The audio buffer may need to be larger than either
         * the original data or the converted data.. The allocated
         * size of buf should be len*len_mult.
         */

        auto temp_buf = new uint8_t[expected_samples * sizeof(mp3d_sample_t) * m_cvt.len_mult];

        size_t read_samples = mp3dec_ex_read(dec, (mp3d_sample_t*)temp_buf,
            expected_samples);

        if (read_samples != expected_samples) {
            if (dec->last_error) {
                return nullptr;
            }
        }

        m_cvt.buf = (Uint8*)temp_buf;
        m_cvt.len = (int)(expected_samples * sizeof(mp3d_sample_t));

        enforce(SDL_ConvertAudio(&m_cvt) == 0,
            "SDL_ConvertAudio");

        samples[i].m_audio_len = (size_t)m_cvt.len_cvt;
        samples[i].m_audio_buf = m_cvt.buf;
    }

    mp3dec_ex_close(dec);

    delete dec;

    return samples;
}

} // thenewworld
