/* Copyright 2020 Nico Sonack
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <SDL2/SDL.h>
#include <cassert>
#include <ctime>
#include <thenewworld/GameContext.hpp>
#include <thenewworld/Logging.hpp>
#include <thenewworld/SDLWindow.hpp>
#include <thenewworld/States/MenuState.hpp>
#include <thenewworld/States/MultiplayerRunningState.hpp>

#include <intro_music.h>

namespace thenewworld {
GameContext::GameContext(bool force_opengl)
    : m_window(std::make_unique<SDLWindow>("The New World", force_opengl))
    , m_audio_player(std::make_unique<SDLAudioPlayer>())
    , m_current_state(std::make_unique<MenuState>())
    , m_rd()
    , m_prng((int)time(NULL))
{
    log_info("Decompressing intro music from embedded mp3");
    m_sound_sample = m_audio_player->load_from_mp3_buffer(___assets_intro_ambient_mp3,
        ___assets_intro_ambient_mp3_len);
    m_tick = [this](const auto& code) {
        if (!this->m_sound_sample || code == boost::asio::error::operation_aborted)
            return;
        m_audio_player->play_sample(this->m_sound_sample);
        this->m_timer->expires_at(this->m_timer->expires_at() + *this->m_interval);
        this->m_timer->async_wait(this->m_tick);
    };

    int ms_time = m_sound_sample->m_audio_len / (sizeof(Uint32) * 44.1);
    m_interval = new boost::posix_time::milliseconds { ms_time };
    m_timer = new boost::asio::deadline_timer { m_io_service, *m_interval };
    m_timer->async_wait(m_tick);
}

GameContext::~GameContext()
{
    log_info("Stopping and deleting timers");
    if (m_timer) {
        // We cannot, for obvious reasons, join a thread that isn't
        // running. Thus, this is an indication to not do this shit
        // and force an abort trap.
        if (m_is_music_running) {
            m_timer->cancel();
            log_info("Joining threads");
            m_io_service.stop();
            m_timer_thread.join();
        }

        delete m_timer;
    }

    if (m_interval)
        delete m_interval;

    SDL_FreeWAV(m_sound_sample->m_audio_buf);
    delete m_sound_sample;
}

int GameContext::run()
{
    m_current_state->init(*this);

    start_background_music();

    // Main Game Loop
    const int delta_time = static_cast<int>((1000.0f / (double)this->fps));

    while (!this->m_should_quit) {
        SDL_Event e;
        // const int begin_frame_time = SDL_GetTicks();

        while (!m_should_quit && SDL_PollEvent(&e) != 0) {

            if (e.type == SDL_QUIT)
                return 0;

            e = m_window->preprocess_sdl_event(e);
            m_current_state->event(*this, e);
        }

        if (m_switching_state) {
            m_switching_state = false;
            continue;
        }

        m_current_state->update(*this, delta_time);

        if (m_switching_state) {
            m_switching_state = false;
            continue;
        }

        m_window->camera().clear_screen();
        m_current_state->render(m_window->camera());
        m_window->present();

        // const int end_frame_time = SDL_GetTicks();

        // assert(end_frame_time >= begin_frame_time);
        SDL_Delay((Uint32)delta_time); // - (end_frame_time - begin_frame_time));
    }

    return 0;
}

void GameContext::start_background_music()
{
    std::thread thread([&]() { m_io_service.run(); });
    m_timer_thread = std::move(thread);
    m_audio_player->play_sample(m_sound_sample);
    m_is_music_running = true;
}

void GameContext::stop_background_music()
{
    if (m_is_music_running) {
        m_timer->cancel();
        log_info("Joining threads");
        m_io_service.stop();
        m_timer_thread.join();
        m_audio_player->stop_playback();
        m_is_music_running = false;
    }
}

void GameContext::quit()
{
    m_should_quit = true;
}

} // thenewworld
